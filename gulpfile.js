"use strict";

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var htmlmin = require('gulp-htmlmin');
var del = require('del');
var replace = require('gulp-replace');
var base64 = require('gulp-base64');
var concat = require('gulp-concat');
var path = require('path');
var merge = require('merge2');
var inject = require('gulp-inject');
var runSequence = require('run-sequence');
var rename = require("gulp-rename");
var empty = require("gulp-empty");
var wrap = require("gulp-wrap");
var gls = require('gulp-live-server');
var inlineImageHtml = require('./inline-image-html/index');
var saiaConfig;

var WRAP_TEMPLATE = '(function(window, $){<%= contents %>})(window, $ || jQuery)';

function applyConfig(stream) {

    Object.keys(saiaConfig)
        .forEach(function (key) {

            stream = stream.pipe(replace(new RegExp('<Config:' + key + '>', 'g'), saiaConfig[key]));
        });

    return stream;
}

function applyMinification() {
    return saiaConfig.env === 'prod' || saiaConfig.env === 'template';
}

gulp.task('clean', function () {
    return del(['dist']);
});

gulp.task('saia.webStore.styles', [], function () {

    return gulp.src(['webStore/src/wp-content/themes/startit/assets/css/web-store-demo.less'])
        .pipe(less())
        .pipe(cssmin())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/css'));
});

gulp.task('saia.webStore.scripts', [], function () {

    return applyConfig(gulp.src([
            'pf-widget/saia-pf-button.js',
            'webStore/src/wp-content/themes/startit/assets/js/web-store-demo.js'
        ]))
        .pipe(wrap(WRAP_TEMPLATE))
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/js'));
});

gulp.task('saia.webStore.assets', [], function () {

    return gulp.src(['webStore/src/wp-content/themes/startit/assets/assets/**/*.*'])
    /*.pipe(imagemin({optimizationLevel: 5}))*/
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/assets'));
});

gulp.task('saia.webStore.index', [], function () {

    return applyConfig(gulp.src(['webStore/src/index.html']))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('dist/webStore'));
});

gulp.task('saia.webStore', [], function (done) {

    runSequence('saia.webStore.styles', 'saia.webStore.scripts', 'saia.webStore.assets', 'saia.webStore.index', done);
});

gulp.task('saia.modelCreator.styles', [], function () {

    return gulp.src(['modelCreator/src/wp-content/themes/startit/assets/css/model-creator.less'])
        .pipe(less())
        .pipe(cssmin())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/css'))
        .pipe(gulp.dest('dist/modelCreator/assets/css'));
});

gulp.task('saia.modelCreator.modelViewer', [], function () {

    return applyConfig(gulp.src([
        'node_modules/three/build/' + (applyMinification() ? 'three.min.js' : 'three.js'),
        'modelViewer/src/libs/Detector.js',
        'modelViewer/src/libs/OBJLoader.js',
        'modelViewer/src/libs/MTLLoader.js',
        'modelViewer/src/libs/OrbitControls.js',
        'modelViewer/src/scripts/model-viewer.js'
    ]))
        .pipe(concat('model-viewer.js'))
        .pipe(applyMinification() ? uglify() : empty())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/js'))
        .pipe(gulp.dest('dist/modelCreator/assets/js'));
});

gulp.task('saia.modelCreator.scripts', [], function () {

    return applyConfig(gulp.src([
        'node_modules/lodash/lodash.js',
        'src/scripts/common.js',
        'src/scripts/core.event-emitter.js',
        'src/scripts/core.js',
        'modelCreator/src/wp-content/themes/startit/assets/js/model-creator.js'
    ]))
        .pipe(concat('model-creator.js'))
        .pipe(wrap(WRAP_TEMPLATE))
        .pipe(applyMinification() ? uglify() : empty())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/js'))
        .pipe(gulp.dest('dist/modelCreator/assets/js'));
});

gulp.task('saia.modelCreator.assets', [], function () {

    return gulp.src(['modelCreator/src/wp-content/themes/startit/assets/assets/**/*.*'])
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/assets'))
        .pipe(gulp.dest('dist/modelCreator/assets/assets'));
});

gulp.task('saia.modelCreator.index', [], function () {

    return applyConfig(gulp.src(['modelCreator/src/index.html']))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('dist/modelCreator'));
});

gulp.task('saia.modelCreator', [], function (done) {

    runSequence(
        'saia.modelCreator.styles',
        'saia.modelCreator.scripts',
        'saia.modelCreator.assets',
        'saia.modelCreator.index',
        'saia.modelCreator.modelViewer',
        done);
});

gulp.task('saia.shopifyDemo.styles', [], function () {

    return gulp.src(['shopifyDemo/src/wp-content/themes/startit/assets/css/shopify-demo.less'])
        .pipe(less())
        .pipe(cssmin())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/css'));
});

gulp.task('saia.shopifyDemo.assets', [], function () {

    return gulp.src(['shopifyDemo/src/wp-content/themes/startit/assets/assets/**/*.*'])
    /*.pipe(imagemin({optimizationLevel: 5}))*/
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/assets'));
});

gulp.task('saia.shopifyDemo.index', [], function () {

    return applyConfig(gulp.src(['shopifyDemo/src/index.html']))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('dist/shopifyDemo'));
});

gulp.task('saia.shopifyDemo', [], function (done) {

    runSequence('saia.shopifyDemo.styles', 'saia.shopifyDemo.assets', 'saia.shopifyDemo.index', done);
});

gulp.task('saia.widget.loader', [], function () {

    return merge([
        applyConfig(gulp.src([
            'node_modules/lodash/lodash.js',
            'src/scripts/common.js',
            'src/scripts/core.event-emitter.js',
            'widget/src/scripts/commands.js',
            'widget/src/scripts/common.js',
            'widget/src/scripts/button.js',
            saiaConfig.env === 'template' ? 'widget/src/scripts/shopify-initializer.js' : ''
        ]))
            .pipe(inject(
                gulp.src(['widget/src/views/**/*.tmpl.html'])
                    .pipe(htmlmin({
                        collapseWhitespace: true,
                        removeComments: true
                    })), {
                    starttag: '/* template:{{path}} */',
                    endtag: '/* endtemplate */',
                    transform: function (filepath, file) {
                        return '\'' + file.contents.toString('utf8') + '\'';
                    }
                }, {
                    removeTags: true
                }))
            .pipe(inject(
                gulp.src(['widget/src/styles/**/*.less'])
                    .pipe(less())
                    .pipe(base64({
                        maxImageSize: Number.MAX_VALUE
                    }))
                    .pipe(cssmin()), {
                    starttag: '/* style:{{path}} */',
                    endtag: '/* endstyle */',
                    transform: function (filepath, file) {
                        return '\'' + file.contents.toString('utf8') + '\'';
                    }
                }, {
                    removeTags: true
                }))
    ])
        .pipe(concat('saia-widget-loader.' + (saiaConfig.env === 'template' ? 'tpl' : 'js')))
        .pipe(wrap(WRAP_TEMPLATE))
        .pipe(applyMinification() ? uglify() : empty())
        .pipe(gulp.dest('dist/wp-content/widget'));
});

gulp.task('saia.widget.modal', [], function () {

    return gulp.src(['widget/src/index.html'])
        .pipe(inject(
            gulp.src(['widget/src/styles/modal.less'])
                .pipe(less())
                .pipe(base64({
                    maxImageSize: Number.MAX_VALUE
                }))
                .pipe(cssmin()), {
                starttag: '<!-- styles -->',
                endtag: '<!-- endstyles -->',
                transform: function (filepath, file) {
                    return '<style>' + file.contents.toString('utf8') + '</style>';
                }
            }, {
                removeTags: true
            }))
        .pipe(inject(
            merge([
                applyConfig(
                    gulp.src([
                        'node_modules/lodash/lodash.js',
                        'src/scripts/common.js',
                        'src/scripts/core.event-emitter.js',
                        'src/scripts/core.js',
                        'widget/src/scripts/commands.js',
                        'widget/src/scripts/common.js',
                        'widget/src/scripts/modal.js'
                    ])
                )
            ])
                .pipe(concat('modal.js'))
                .pipe(wrap(WRAP_TEMPLATE))
                .pipe(applyMinification() ? uglify() : empty()), {
                starttag: '<!-- scripts -->',
                endtag: '<!-- endscripts -->',
                transform: function (filepath, file) {
                    return '<script type="application/javascript">' + file.contents.toString('utf8') + '</script>';
                }
            }, {
                removeTags: true
            }))
        .pipe(inlineImageHtml(path.join(__dirname, 'widget/src')))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(rename('modal.' + (saiaConfig.env === 'template' ? 'tpl' : 'html')))
        .pipe(gulp.dest('dist/wp-content/widget'));
});

gulp.task('saia.widget', [], function (done) {

    runSequence('saia.widget.loader', 'saia.widget.modal', done);
});

gulp.task('webStore:build:dev', ['clean'], function (done) {

    saiaConfig = require('./saia-config.dev');

    runSequence('saia.webStore', done);
});

gulp.task('webStore:build:prod', ['clean'], function (done) {

    saiaConfig = require('./saia-config.prod');

    runSequence('saia.webStore', done);
});

gulp.task('webStore:serve', [], function (done) {

    runSequence('webStore:build:dev', function () {

        var server = gls.static('dist', 8084);
        server.start();

        gulp.watch(['src/**/*.*', 'webStore/src/**/*.*', 'widget/src/**/*.*'], function (file) {

            runSequence('webStore:build:dev', function () {

                server.notify.apply(server, [file]);
            });
        });

        done();
    });
});

gulp.task('modelCreator:build:dev', ['clean'], function (done) {

    saiaConfig = require('./saia-config.dev');

    runSequence('saia.modelCreator', done);
});

gulp.task('modelCreator:build:prod', ['clean'], function (done) {

    saiaConfig = require('./saia-config.prod');

    runSequence('saia.modelCreator', done);
});

gulp.task('modelCreator:build:test', ['clean'], function (done) {

    saiaConfig = require('./saia-config.test');

    runSequence('saia.modelCreator', done);
});

gulp.task('modelCreator:serve', [], function (done) {

    saiaConfig = require('./saia-config.dev');

    runSequence('modelCreator:build:dev', function () {

        var server = gls.static('dist', 8084);
        server.start();

        gulp.watch(['src/**/*.*', 'modelCreator/src/**/*.*', 'modelViewer/src/**/*.*'], function (file) {

            runSequence('modelCreator:build:dev', function () {

                server.notify.apply(server, [file]);
            });
        });

        done();
    });
});

gulp.task('shopifyDemo:build:dev', ['clean'], function (done) {

    saiaConfig = require('./saia-config.dev');

    runSequence('saia.shopifyDemo', 'saia.widget', done);
});

gulp.task('shopifyDemo:build:prod', ['clean'], function (done) {

    saiaConfig = require('./saia-config.prod');

    runSequence('saia.shopifyDemo', 'saia.widget', done);
});

gulp.task('shopifyDemo:build:template', ['clean'], function (done) {

    saiaConfig = require('./saia-config.template');

    runSequence('saia.shopifyDemo', 'saia.widget', done);
});

gulp.task('shopifyDemo:serve', [], function (done) {

    runSequence('shopifyDemo:build:dev', function () {

        var server = gls.static('dist', 8084);
        server.start();

        gulp.watch(['src/**/*.*', 'shopifyDemo/src/**/*.*', 'widget/src/**/*.*'], function (file) {

            runSequence('shopifyDemo:build:dev', function () {

                server.notify.apply(server, [file]);
            });
        });

        done();
    });
});

gulp.task('saia.liveModelViewer.styles', [], function () {

    return gulp.src(['liveModelViewer/src/wp-content/themes/startit/assets/css/live-model-viewer.less'])
        .pipe(less())
        .pipe(cssmin())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/css'));
});

gulp.task('saia.liveModelViewer.scripts', [], function () {

    return applyConfig(gulp.src([
        'node_modules/lodash/lodash.js',
        'node_modules/three/build/three.js',
        'modelViewer/src/libs/Detector.js',
        'modelViewer/src/libs/OBJLoader.js',
        'modelViewer/src/libs/MTLLoader.js',
        'modelViewer/src/libs/OrbitControls.js',
        'src/scripts/common.js',
        'modelViewer/src/scripts/model-viewer.js',
        'liveModelViewer/src/wp-content/themes/startit/assets/js/live-model-viewer.js'
    ]))
        .pipe(concat('live-model-viewer.js'))
        .pipe(wrap(WRAP_TEMPLATE))
        .pipe(applyMinification() ? uglify() : empty())
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/js'))
        .pipe(gulp.dest('dist/liveModelViewer/assets/js'));
});

gulp.task('saia.liveModelViewer.assets', [], function () {

    return gulp.src(['liveModelViewer/src/wp-content/themes/startit/assets/assets/**/*.*'])
        .pipe(gulp.dest('dist/wp-content/themes/startit/assets/assets'));
});

gulp.task('saia.liveModelViewer.index', [], function () {

    return applyConfig(gulp.src(['liveModelViewer/src/index.html']))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('dist/liveModelViewer'));
});

gulp.task('saia.liveModelViewer', [], function (done) {

    runSequence(
        'saia.liveModelViewer.styles',
        'saia.liveModelViewer.scripts',
        'saia.liveModelViewer.assets',
        'saia.liveModelViewer.index',
        done
    );
});

gulp.task('liveModelViewer:build:dev', ['clean'], function (done) {

    saiaConfig = require('./saia-config.dev');

    runSequence('saia.liveModelViewer', done);
});

gulp.task('liveModelViewer:build:prod', ['clean'], function (done) {

    saiaConfig = require('./saia-config.prod');

    runSequence('saia.liveModelViewer', done);
});

gulp.task('liveModelViewer:serve', [], function (done) {

    runSequence('liveModelViewer:build:dev', function () {

        var server = gls.static('dist', 8084);
        server.start();

        gulp.watch(['src/**/*.*', 'liveModelViewer/src/**/*.*', 'modelViewer/src/**/*.*'], function (file) {

            runSequence('liveModelViewer:build:dev', function () {

                server.notify.apply(server, [file]);
            });
        });

        done();
    });
});
