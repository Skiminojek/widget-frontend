'use strict';

(function (saia) {

    (function (widget) {

        var Command = widget.Command = {
            LOADING: 'saia.widget.loading',
            LOADED: 'saia.widget.loaded',
            RESULT_RECEIVED: 'saia.widget.resultReceived',
            CLOSED: 'saia.widget.closed'
        };

    })(saia.widget || (saia.widget = {}));

})(window.saia || (window.saia = {}));