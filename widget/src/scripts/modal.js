'use strict';

(function (parent) {

    var API_ENDPOINT = '<Config:apiEndpoint>';
    var SIZE_DETECTOR_API_ENDPOINT = '<Config:sizeDetectorApiEndpoint>';
    var SHOP_NAME = '{{shopName}}';

    var Model = saia.core.Model;
    var GenderSwitcher = saia.core.GenderSwitcher;
    var HeightInput = saia.core.HeightInput;
    var Photo = saia.core.Photo;
    var PhotoGroup = saia.core.PhotoGroup;
    var Uploader = saia.core.Uploader2;
    var Command = saia.widget.Command;

    var common = saia.common;
    var model = new Model({
        gender: 'female',
        height: 0,
        frontPhoto: null,
        sidePhoto: null
    });
    var apiKey = getParameterByName('apiKey');
    var sizeDetectorApiKey = getParameterByName('sizeDetectorApiKey');
    var href = getParameterByName('href');
    var origin = getParameterByName('origin');
    var template = {
        female: JSON.parse(
            getParameterByName('femaleTemplate')
        ),
        male: JSON.parse(
            getParameterByName('maleTemplate')
        )
    };

    if (!template.female || !template.male) {

        template = null;
    }

    function getParameterByName(name, url) {

        if (!url) {

            url = window.location.href;
        }

        name = name.replace(/[\[\]]/g, "\\$&");

        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);

        if (!results) {

            return null;
        }

        if (!results[2]) {

            return '';
        }

        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function showLoader(label) {

        $('.loader-label').text(label || '');
        $('.loader').removeClass('hidden');
    }

    function hideLoader() {

        $('.loader').addClass('hidden');
    }

    function close() {

        parent.postMessage({
            command: Command.CLOSED
        }, origin);
    }

    $(window).on('load', function () {

        (function () {

            model.set('gender', (function () {

                var gender = getParameterByName('gender');

                if (['female', 'male'].indexOf(gender) === -1) {

                    gender = 'female';
                }

                return gender;
            })());

            $(document).keyup(function (e) {

                if (e.keyCode === 27) {

                    close();
                }
            });

            /* logo */
            (function () {

                var logoTimeout = +getParameterByName('logoTimeout');
                if (logoTimeout) {

                    $('.logo').removeClass('hidden');

                    setTimeout(function () {

                        $('.logo').addClass('hidden');
                        $('.wrapper').removeClass('invisible');
                    }, logoTimeout);
                } else {

                    $('.wrapper').removeClass('invisible');
                }
            })();

            /* header */
            (function () {

                var help = $('.help');
                var wrapper = $('.wrapper');
                var showHelpLabel = $('.show-help');
                var backLabel = $('.back');
                var body = $('body');

                showHelpLabel.click(function () {

                    help.removeClass('hidden');
                    wrapper.addClass('invisible');
                    showHelpLabel.addClass('help-shown');
                    backLabel.removeClass('hidden');
                    body.addClass('without-overflow');
                });

                ['.hide-help', '.back'].forEach(function (selector) {

                    $(selector).click(function () {

                        help.addClass('hidden');
                        wrapper.removeClass('invisible');
                        showHelpLabel.removeClass('help-shown');
                        backLabel.addClass('hidden');
                        body.removeClass('without-overflow');
                    });
                });

                $('.close-modal').click(function () {

                    close();
                });

                $('.close-modal .background').click(function (e) {

                    if (!common.isMobileDevice()) {

                        e.stopPropagation();
                    }
                });

                $('.invisible-clickable-area').click(function (e) {

                    if (!common.isMobileDevice()) {

                        e.stopPropagation();
                    }
                });
            })();

            /* content */
            (function () {

                var timestamp = +getParameterByName('timestamp');
                var itemName = getParameterByName('itemName');
                var brand = getParameterByName('brand');
                var genderControlsStyle = getParameterByName('genderControlsStyle');

                if (timestamp) {

                    var date = new Date(timestamp);
                    var dateString = (date.getMonth() + 1) + '.' + date.getDate() + '.' + date.getFullYear();

                    $('.instruction').text('We determined your size on ' + dateString + '. If you want to update your parameters you should reupload the photos.');
                }

                /* video guide */
                (function () {

                    $('.video-guide .next-step').click(function () {

                        $('.video-guide iframe').prop('src', '');
                        $('.video-guide').addClass('hidden');
                        $('.start').removeClass('hidden');
                        $('.height-input-cm').focus();

                        switch (genderControlsStyle) {
                            case 'buttons': {

                                $('.gender.buttons').removeClass('hidden');
                                break;
                            }
                            case 'icons': {

                                $('.gender.icons').removeClass('hidden');
                                break;
                            }
                            default: {

                                $('.gender.buttons').removeClass('hidden');
                            }
                        }
                    });
                })();

                /* startPage */
                (function () {

                    var cmInput = $('.height-input-cm');
                    var ftInput = $('.height-input-ft');
                    var inInput = $('.height-input-in');
                    var genderSwitcher = new GenderSwitcher(
                        $('.female'), $('.male'), model.get('gender')
                    );
                    var heightInput = new HeightInput(
                        $('.height'), cmInput, ftInput, inInput, $('.cm'), $('.in')
                    );
                    var nextStepButton = $('.start .next-step');

                    genderSwitcher.on('change', function (gender) {

                        model.set('gender', gender);
                        heightInput.focus();
                    });

                    heightInput.on('change', function (height) {

                        model.set('height', height);
                    });

                    heightInput.on('unit:change', function () {

                        heightInput.focus();
                    });

                    cmInput.keydown(function (e) {

                        if (e.keyCode === 13) {

                            nextStepButton.click();

                            setTimeout(function () {

                                cmInput.blur();
                            }, 0);
                        }
                    });

                    inInput.keydown(function (e) {

                        if (e.keyCode === 13) {

                            nextStepButton.click();

                            setTimeout(function () {

                                inInput.blur();
                            }, 0);
                        }
                    });

                    nextStepButton.click(function () {

                        if (heightInput.isValid()) {

                            $('.start').addClass('hidden');
                            $('.photos-upload').removeClass('hidden');
                        } else {

                            alert('Please provide correct height');
                        }
                    });
                })();

                /* photos upload */
                (function () {

                    var frontPhoto = new Photo(
                        $('.front-frame'),
                        $('.front-frame .photo-wrapper'),
                        $('.front-frame .photo-wrapper .drag-overlay'),
                        $('.front-frame .photo-wrapper .invalid-photo-overlay'),
                        $('.front-frame .photo-wrapper .photo')
                    );
                    var sidePhoto = new Photo(
                        $('.side-frame'),
                        $('.side-frame .photo-wrapper'),
                        $('.side-frame .photo-wrapper .drag-overlay'),
                        $('.side-frame .photo-wrapper .invalid-photo-overlay'),
                        $('.side-frame .photo-wrapper .photo')
                    );
                    var photoGroup = new PhotoGroup(
                        $('.photos-wrapper'), frontPhoto, sidePhoto
                    );
                    var uploader = new Uploader(apiKey, API_ENDPOINT, model);

                    var nextStepButton = $('.photos-upload .next-step');
                    var isAgreementChecked = false;

                    if (template) {

                        $('.template')
                            .removeClass('hidden')
                            .click(function () {

                                var genderTemplate = template[model.get('gender')];

                                model.set('height', genderTemplate.height);

                                common.deferredTask(
                                    $.when(
                                        frontPhoto.fromURL(genderTemplate.front, 'front'),
                                        sidePhoto.fromURL(genderTemplate.side, 'side')
                                    ), function () {

                                        showLoader('Loading template');
                                    }, 250
                                ).then(function () {

                                    hideLoader();
                                });
                            });
                    }

                    $('#min-photo-size').text(
                        common.formatFileSize(Photo.MIN_PHOTO_SIZE)
                    );

                    $('#max-photo-size').text(
                        common.formatFileSize(Photo.MAX_PHOTO_SIZE)
                    );

                    $('#agreement-checkbox').change(function () {

                        isAgreementChecked = this.checked;

                        if (isAgreementChecked) {

                            nextStepButton.removeClass('disabled');
                        } else {

                            nextStepButton.addClass('disabled');
                        }
                    });

                    [frontPhoto, sidePhoto].forEach(function (photo) {

                        photo.on('change', function (loadTask) {

                            loadTask.fail(function (e) {

                                alert(e);
                            });

                            common.deferredTask(loadTask, function () {

                                showLoader('Loading photo');
                            }, 250).then(function () {

                                hideLoader();
                            });
                        });
                    });

                    frontPhoto.on('load', function () {

                        model.set('frontPhoto', frontPhoto.binaryData);
                    });

                    sidePhoto.on('load', function () {

                        model.set('sidePhoto', sidePhoto.binaryData);
                    });

                    nextStepButton.click(function () {

                        if (!isAgreementChecked) {

                            return;
                        }

                        if (!frontPhoto.isValid()) {

                            alert('Please provide valid front photo');
                            return;
                        }

                        if (!sidePhoto.isValid()) {

                            alert('Please provide valid side photo');
                            return;
                        }

                        function onCompleteResponseReceived(completeResponse) {

                            (function () {

                                if (sizeDetectorApiKey) {
                                    
                                    return saia.widget.common.getSize(sizeDetectorApiKey, SIZE_DETECTOR_API_ENDPOINT, {
                                        height: completeResponse.height,
                                        gender: completeResponse.gender,
                                        hips: completeResponse.hips,
                                        chest: completeResponse.chest,
                                        waist: completeResponse.waist,
                                        url: href,
                                        shop_name: SHOP_NAME,
                                    });
                                }

                                return $.when(
                                    common.calculateSize(completeResponse.clothSizes, brand)
                                );
                            })().then(function (size) {

                                hideLoader();

                                $('.photos-upload').addClass('hidden');
                                $('.result').removeClass('hidden');
                                $('.item-name').text(itemName);

                                if (size) {

                                    parent.postMessage({
                                        command: Command.RESULT_RECEIVED,
                                        result: {
                                            size: size,
                                            gender: model.get('gender'),
                                            completeResponse: completeResponse
                                        }
                                    }, origin);

                                    $('.size').text(size);
                                    $('.size-not-found').addClass('hidden');
                                } else {

                                    $('.size-found, .fit-available, .suggestion').addClass('hidden');
                                }
                            }, function () {

                                hideLoader();
                                $('.size').text('???');
                            });
                        }

                        function handleUploadError(e) {
                            if (e && e.body && e.body.sub_tasks) {
                                var subTasks = e.body.sub_tasks;
                                var front = subTasks.filter(function (s) { return s.name.indexOf('front_') !== -1 })[0];
                                var side = subTasks.filter(function (s) { return s.name.indexOf('side_') !== -1 })[0];

                                var errorMessage = '';

                                if (front.status === 'FAILURE') {
                                    errorMessage += 'Front photo: ' + front.message + '\n'
                                }

                                if (side.status === 'FAILURE') {
                                    errorMessage += 'Side photo: ' + side.message + '\n'
                                }

                                if (errorMessage) {
                                    alert(errorMessage);
                                }
                            } else {
                                alert('Error');
                            }
                        }

                        showLoader('Processing');

                        uploader.upload()
                            .then(function (response) {

                                onCompleteResponseReceived({
                                    clothSizes: _.get(response, 'cloth_sizes'),
                                    height: +model.get('height'),
                                    gender: model.get('gender'),
                                    chest: Math.round(_.get(response, 'volume_params.chest')),
                                    hips: Math.round(_.get(response, 'volume_params.high_hips')),
                                    waist: Math.round(_.get(response, 'volume_params.waist')),
                                    timestamp: _.now()
                                });
                            }, function (e) {
                                handleUploadError(e);
                                hideLoader();
                            });
                    });
                })();

                /* result */
                (function () {

                    $('.result .next-step').click(function () {

                        close();
                    });
                })();
            })();
        })();

        parent.postMessage({
            command: Command.LOADED
        }, origin);
    });

    parent.postMessage({
        command: Command.LOADING
    }, origin);
})(parent);