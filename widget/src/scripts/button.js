'use strict';

(function (saia) {

    var API_ENDPOINT = '<Config:apiEndpoint>';
    var SIZE_DETECTOR_API_ENDPOINT = '<Config:sizeDetectorApiEndpoint>';
    var SHOP_NAME = '{{shopName}}';

    var common = saia.common;

    (function (widget) {

        var BUTTON_STYLE = /* style:/widget/src/styles/button.css */''/* endstyle */;
        var MODAL_BACKDROP_STYLE = /* style:/widget/src/styles/modal-backdrop.css */''/* endstyle */;
        var BUTTON_TEMPLATE = /* template:/widget/src/views/button.tmpl.html */''/* endtemplate */;
        var MODAL_BACKDROP_TEMPLATE = /* template:/widget/src/views/modal-backdrop.tmpl.html */''/* endtemplate */;

        var Command = widget.Command;

        var buttonTemplate = $(BUTTON_TEMPLATE);
        var modalBackdropTemplate = $(MODAL_BACKDROP_TEMPLATE);

        function injectStyle(source, id) {

            if ($('#' + id).length === 0) {

                $('<style type="text/css" id="' + id + '">' + source + '</style>')
                    .appendTo('head');
            }
        }

        widget.Button = (function () {

            var EventEmitter = saia.core.EventEmitter;

            saia.common.extend(Button, EventEmitter);

            function Button(container) {

                var self = this;

                EventEmitter.call(self);

                $(container).hide();

                var options = {
                    itemName: $(container).data('item-name'),
                    brand: $(container).data('brand'),
                    logoTimeout: +$(container).data('logo-timeout') || 0,
                    buttonStyle: $(container).data('button-style') || 'b-2',
                    genderControlsStyle: $(container).data('gender-controls-style') || 'icons',
                    gender: $(container).data('gender'),
                    femaleTemplate: $(container).data('female-template'),
                    maleTemplate: $(container).data('male-template'),
                    apiKey: '<Config:apiKey>' || $(container).data('api-key'),
                    sizeDetectorApiKey: '<Config:apiKey>' || $(container).data('size-detector-api-key')
                };

                saia.widget
                    .common
                    .getWidgetVisibility(options.apiKey, SIZE_DETECTOR_API_ENDPOINT, location.href)
                    .then(function (isWidgetVisible) {
                        
                        if (isWidgetVisible) {

                            var buttonElement = buttonTemplate.clone();

                            $(container).show();

                            if (options.buttonStyle) {

                                buttonElement.addClass(options.buttonStyle);
                                $(container).append(buttonElement);
                            }

                            $(container).click(function () {

                                var modalBackdrop = modalBackdropTemplate.clone();
                                var url = '<Config:widgetURL>/modal.html?key=<Config:apiKey>';
                                var completeResponse = common.getCompleteResponse();

                                function appendQueryParam(key, value) {

                                    if (url.indexOf('?') !== -1) {

                                        url += '&' + key + '=' + encodeURIComponent(value);
                                    } else {

                                        url += '?' + key + '=' + encodeURIComponent(value);
                                    }
                                }

                                if (options.itemName) {

                                    appendQueryParam('itemName', options.itemName);
                                }

                                if (options.brand) {

                                    appendQueryParam('brand', options.brand);
                                }

                                if (options.genderControlsStyle) {

                                    appendQueryParam('genderControlsStyle', options.genderControlsStyle);
                                }

                                if (options.gender) {

                                    appendQueryParam('gender', options.gender);
                                }

                                if (options.logoTimeout) {

                                    appendQueryParam('logoTimeout', options.logoTimeout);
                                }

                                if (options.femaleTemplate && options.maleTemplate) {

                                    var getTemplate = function (s) {

                                        var template = {};

                                        s.split(' ')
                                            .forEach(function (item) {

                                                var row = item.split('=');
                                                template[row[0]] = row[1];
                                            });

                                        return JSON.stringify(template);
                                    };

                                    appendQueryParam('femaleTemplate', getTemplate(options.femaleTemplate));
                                    appendQueryParam('maleTemplate', getTemplate(options.maleTemplate));
                                } else {

                                    appendQueryParam('femaleTemplate', 'null');
                                    appendQueryParam('maleTemplate', 'null');
                                }

                                if (options.apiKey) {

                                    appendQueryParam('apiKey', options.apiKey);
                                }

                                if (options.sizeDetectorApiKey) {

                                    appendQueryParam('sizeDetectorApiKey', options.sizeDetectorApiKey);
                                }

                                if (completeResponse) {

                                    appendQueryParam('timestamp', completeResponse.timestamp);
                                }

                                appendQueryParam('href', location.href);
                                appendQueryParam('origin', location.origin);

                                modalBackdrop.find('iframe')
                                    .attr('src', url);

                                setTimeout(function () {

                                    function destroyModal() {

                                        $(window).off('keyup', onKeyUp);
                                        $(window).off('message', onMessage);
                                        modalBackdrop.remove();
                                    }

                                    function onKeyUp(e) {

                                        if (e.keyCode === 27) {

                                            window.postMessage({
                                                command: Command.CLOSED
                                            }, location.origin);

                                            destroyModal();
                                        }
                                    }

                                    $(window).on('keyup', onKeyUp);

                                    function onMessage(event) {

                                        var messageEvent = event.originalEvent;

                                        switch (messageEvent.data.command) {
                                            case Command.LOADED: {

                                                modalBackdrop.find('iframe')
                                                    .removeClass('invisible');
                                                break;
                                            }
                                            case Command.RESULT_RECEIVED: {

                                                var result = messageEvent.data.result;

                                                if (options.buttonStyle && options.buttonStyle === 'demo') {

                                                    $(container).text('Your size: ' + result.size);
                                                } else if (options.buttonStyle) {

                                                    buttonElement.find('.sw-text')
                                                        .text('Your size: ' + result.size);
                                                } else {

                                                    $(container).text('Your size: ' + result.size);
                                                }

                                                common.setCompleteResponse(result.completeResponse);
                                                self.emit('size', result.size, result.gender);
                                                break;
                                            }
                                            case Command.CLOSED: {

                                                destroyModal();
                                                break;
                                            }
                                        }
                                    }

                                    $(window).on('message', onMessage);
                                    modalBackdrop.click(function () {

                                        window.postMessage({
                                            command: Command.CLOSED
                                        }, location.origin);

                                        destroyModal();
                                    });

                                    $(document.body).append(modalBackdrop);
                                }, 0);
                            });

                            injectStyle(BUTTON_STYLE, 'saia-button-style');
                            injectStyle(MODAL_BACKDROP_STYLE, 'saia-modal-backdrop-style');

                            (function () {

                                var completeResponse = common.getCompleteResponse();

                                if (options.sizeDetectorApiKey) {

                                    if (completeResponse) {

                                        return saia.widget.common.getSize(options.sizeDetectorApiKey, SIZE_DETECTOR_API_ENDPOINT, {
                                            height: completeResponse.height,
                                            gender: completeResponse.gender,
                                            hips: completeResponse.hips,
                                            chest: completeResponse.chest,
                                            waist: completeResponse.waist,
                                            url: location.href
                                        });
                                    }

                                    return $.when(null);
                                }

                                return $.when(
                                    common.getSize(options.brand)
                                );
                            })().then(function (size) {

                                if (size) {

                                    if (options.buttonStyle && options.buttonStyle === 'demo') {

                                        $(container).text('Your size: ' + size);
                                    } else if (options.buttonStyle) {

                                        buttonElement.find('.sw-text')
                                            .text('Your size: ' + size);
                                    } else {

                                        $(container).text('Your size: ' + size);
                                    }

                                    self.emit('size', size, options.gender);
                                }
                            });
                        }
                    });
            }

            return Button;
        })();

    })(saia.widget || (saia.widget = {}));

})(window.saia || (window.saia = {}));
