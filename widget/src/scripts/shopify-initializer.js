var saiaCont = document.querySelector('.saia-widget-container');

if (saiaCont) {

    document
        .querySelectorAll('.saia-widget-container')
        .forEach(function (container) {
            new saia.widget.Button(container)
                .on('size', function (size, gender) {
                    console.log('size: ' + size);
                });
        });
} else {

    var cartAdd = document.querySelector("form[action='/cart/add']");
    var cont = document.createElement('div');
    cont.className = 'saia-widget-container';
    var parentDiv = cartAdd.parentNode;
    parentDiv.insertBefore(cont, cartAdd);
    new saia.widget.Button(cont)
        .on('size', function (size, gender) {
            console.log('size: ' + size);
        });
}