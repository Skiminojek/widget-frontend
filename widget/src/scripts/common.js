'use strict';

(function (saia) {

    (function (widget) {

        widget.common = {
            getSize: function (apiKey, apiEndpoint, data) {

                if ('<Config:env>' === 'dev') {

                    data.url = 'https://3dlook.me/webdemoproduct';
                }

                return $.ajax({
                    type: 'POST',
                    url: apiEndpoint + '/products/product/get-size/',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data),
                    headers: {
                        'Authorization': 'APIKey ' + apiKey
                    },
                    dataType: 'json'
                }).then(function (response) {

                    if (response && response.size) {

                        return response.size;
                    }

                    return null;
                });
            },
            getWidgetVisibility: function (apiKey, apiEndpoint, productUrl) {
                if ('<Config:env>' === 'dev') {
                    productUrl = 'https://3dlook.me/webdemoproduct';
                }

                return $.ajax({
                    type: 'GET',
                    url: apiEndpoint + '/products/product/?url=' + encodeURIComponent(productUrl),
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        'Authorization': 'APIKey ' + apiKey
                    }
                }).then(function (response) {

                    if (response) {

                        return response['widget_is_visible'];
                    }

                    return false;
                });
            }
        };

    })(saia.widget || (saia.widget = {}));

})(window.saia || (window.saia = {}));
