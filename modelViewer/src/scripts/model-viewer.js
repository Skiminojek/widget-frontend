'use strict';

(function (saia, $) {

    var GRID_HELPER_SIZE = 2000;
    var GRID_HELPER_DIVISIONS = 80;

    var getMaterialsPromise = null;

    var State = {
        CREATED: 'created',
        INITIALIZED: 'initialized',
        DESTROYED: 'destroyed'
    };

    var ModelViewer = saia.ModelViewer = (function () {

        function ModelViewer() {
            this._container = undefined;
            this.camera = undefined;
            this.scene = undefined;
            this.renderer = undefined;
            this.controls = undefined;
            this.modelURL = undefined;
            this._state = State.CREATED;
            this._isModelLoading = false;
        }

        ModelViewer.prototype.init = function (container) {

            var self = this;

            self._container = $(container);

            (function () {

                var container = self._container;

                var camera = self.camera = new THREE.PerspectiveCamera(50, container.width() / container.height(), 1, 10000000);

                var scene = self.scene = new THREE.Scene();
                scene.fog = new THREE.FogExp2(0xE7E8EB, 0.015);

                var renderer = self.renderer = new THREE.WebGLRenderer({antialias: true});
                renderer.setPixelRatio(window.devicePixelRatio);
                renderer.setSize(container.width(), container.height());
                renderer.setClearColor(new THREE.Color(0xE7E8EC));
                renderer.shadowMap.enabled = true;

                container.append(renderer.domElement);

                var controls = new THREE.OrbitControls(camera, renderer.domElement);
                controls.enableDamping = true;
                controls.dampingFactor = 0.25;
                controls.enableZoom = true;

                function onWindowResize() {

                    var width = container.width();
                    var height = container.height();

                    camera.aspect = width / height;
                    camera.updateProjectionMatrix();
                    renderer.setSize(width, height);
                }

                function animate() {

                    if (self._state === State.DESTROYED) {

                        $(window).off('resize', onWindowResize);
                        return;
                    }

                    requestAnimationFrame(animate);
                    controls.update();
                    renderer.render(scene, camera);
                }

                animate();

                $(window).on('resize', onWindowResize);

                self._state = State.INITIALIZED;
            })();
        };

        ModelViewer.prototype.load = function (modelURL) {
            console.log('modelURL ' + modelURL);
            var self = this;

            return $.Deferred(function (deferred) {

                switch (self._state) {
                    case State.CREATED: {

                        deferred.reject(
                            new Error('Model viewer is not initialized')
                        );
                        break;
                    }
                    case State.DESTROYED: {

                        deferred.reject(
                            new Error('Model viewer has been destroyed')
                        );
                        break;
                    }
                    default: {

                        // self.modelURL = modelURL.replace('http', 'https');
                        self._isModelLoading = true;

                        loadModel(modelURL).then(function (model) {

                            initModel(model, self.camera, self.scene);

                            self._isModelLoading = false;
                            deferred.resolve();
                        }, function (error) {

                            self._isModelLoading = false;
                            deferred.reject(error);
                        });
                    }
                }
            }).promise();
        };

        ModelViewer.prototype.destroy = function () {

            var self = this;

            self._state = State.DESTROYED;
            self._container.empty();
        };

        return ModelViewer;
    })();

    function loadModel(modelURL) {
        // modelURL = modelURL.replace('http', 'https');
        return $.Deferred(function (deferred) {

            (getMaterialsPromise || (getMaterialsPromise = $.Deferred(function (deferred) {

                var mtlLoader = new THREE.MTLLoader();
                mtlLoader.setBaseUrl('/wp-content/themes/startit/assets/assets/');
                mtlLoader.setPath('/wp-content/themes/startit/assets/assets/');
                mtlLoader.load('out_scene.mtl', function (materials) {

                    materials.preload();
                    deferred.resolve(materials);
                }, function () {
                }, function () {

                    deferred.resolve();
                });
            }).promise())).then(function (materials) {

                var objLoader = new THREE.OBJLoader();

                if (materials) {

                    objLoader.setMaterials(materials);
                }

                objLoader.load(modelURL, function (object) {

                    _.each(object.children, function (mesh) {

                        mesh.material = new THREE.MeshLambertMaterial({
                            color: new THREE.Color(0xADADAD),
                            emissive: new THREE.Color(0x737373),
                            shading: THREE.FlatShading,
                            polygonOffset: true,
                            polygonOffsetFactor: 1,
                            polygonOffsetUnits: 1,
                            fog: false,
                            side: THREE.DoubleSide
                        });

                        var wireframe = new THREE.LineSegments(
                            new THREE.WireframeGeometry(mesh.geometry),
                            new THREE.LineBasicMaterial({
                                color: 0x5f5f5f,
                                linewidth: 1,
                                fog: false
                            })
                        );

                        mesh.add(wireframe);
                    });

                    deferred.resolve(object);
                }, function () {
                }, function (error) {

                    deferred.reject(error);
                });
            }, function (error) {

                deferred.reject(error);
            });
        }).promise();
    }

    function initModel(object, camera, scene) {

        while (scene.children.length > 0) {

            scene.remove(scene.children[0]);
        }

        scene.add(object);

        var box = new THREE.Box3().setFromObject(object);
        box.getCenter(object.position);
        object.localToWorld(box);
        object.position.multiplyScalar(-1);

        var pos = scene.position;
        var sceneBox = new THREE.Box3().setFromObject(scene);
        var height = Math.max(sceneBox.getSize().y, sceneBox.getSize().x);
        var fov = camera.fov * (Math.PI / 180);
        var distance = Math.abs(height / Math.sin(fov / 2));

        camera.position.set(pos.x, pos.y, distance + (height / 2));
        camera.zoom = 2.5;
        camera.updateProjectionMatrix();

        var gridHelper = new THREE.GridHelper(height, GRID_HELPER_DIVISIONS);
        gridHelper.position.y -= (height / 2);
        gridHelper.rotation.y = Math.PI / 4;

        scene.add(gridHelper);

        var light = new THREE.HemisphereLight(0xeeeeee, 0x888888, 0.5);
        light.position.set(0, 20, 0);

        scene.add(light);
    }

    if (!Detector.webgl) {

        Detector.addGetWebGLMessage();
    }

})(window.saia || (window.saia = {}), window.jQuery || window.$);
