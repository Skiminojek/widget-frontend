'use strict';

(function (saia) {

    saia.common = {
        getCompleteResponse: function () {

            try {

                var completeResultJSON = localStorage.getItem('saiaWidget.completeResponse');
                return JSON.parse(completeResultJSON) || null;
            } catch (e) {

                return null;
            }
        },
        getSize: function (brand) {

            var completeResponse = this.getCompleteResponse();
            if (completeResponse) {

                return this.calculateSize(completeResponse.clothSizes, brand);
            }

            return null;
        },
        setCompleteResponse: function (completeResponse) {

            if (completeResponse) {

                localStorage.setItem('saiaWidget.completeResponse', JSON.stringify(completeResponse));
            }
        },
        calculateSize: function (clothSizes, brand) {

            function getClothSizeFactor(size) {

                size = size.toUpperCase();

                if (size === 'S') {

                    return -1;
                }

                if (size === 'M') {

                    return 0;
                }

                if (size === 'L') {

                    return 1;
                }

                if (/^X*[SL]$/.test(size)) {

                    if (size[size.length - 1] === 'S') {

                        return -size.length;
                    }

                    if (size[size.length - 1] === 'L') {

                        return +size.length;
                    }
                }

                if (/^\dX[SL]$/.test(size)) {

                    if (size[size.length - 1] === 'S') {

                        return -(+size[0] + 1);
                    }

                    if (size[size.length - 1] === 'L') {

                        return +(+size[0] + 1);
                    }
                }

                return NaN;
            }

            if (clothSizes.length > 0) {

                if (brand) {

                    var clothSize = _.find(clothSizes, function (clothSize) {

                        return clothSize.brand.toLowerCase() === brand.toLowerCase();
                    });

                    if (clothSize) {

                        return clothSize.size_alpha;
                    }
                }

                var sizeAmountMap = _.mapValues(
                    _.groupBy(clothSizes, 'size_alpha'), function (clothSizes) {
                        return clothSizes.length;
                    }
                );
                var sizes = _.keys(sizeAmountMap);

                if (sizes.length !== 0) {

                    var size = _.maxBy(sizes, function (size) {

                        return sizeAmountMap[size];
                    });

                    var suitableSizes = _.map(
                        _.pickBy(sizeAmountMap, function (amount) {

                            return amount === sizeAmountMap[size];
                        }), function (value, key) {

                            return key;
                        }
                    );

                    return _.maxBy(suitableSizes, getClothSizeFactor);
                }

                return null;
            }

            return null;
        },
        isMobileDevice: function () {

            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        },
        deferredTask: function (promise, task, delay) {

            var defer = $.Deferred();
            var isCompleted = false;
            var isTaskExecuted = false;

            promise.then(function () {

                isCompleted = true;
                if (isTaskExecuted) {

                    defer.resolve();
                }
            }, function () {

                isCompleted = true;
                if (isTaskExecuted) {

                    defer.resolve();
                }
            });

            setTimeout(function () {

                if (!isCompleted) {

                    task();
                    isTaskExecuted = true;
                }
            }, delay || 0);

            return defer.promise();
        },
        formatFileSize: function (size) {

            var i = Math.floor(Math.log(size) / Math.log(1024));
            return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'Kb', 'Mb', 'Gb', 'Tb'][i];
        },
        extend: function (a, b) {

            function __() {
                this.constructor = a;
            }

            __.prototype = b.prototype;

            a.prototype = new __();
        }
    };

})(window.saia || (window.saia = {}));