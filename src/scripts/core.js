'use strict';

(function (saia) {

    var common = saia.common;

    (function (core) {

        var extend = saia.common.extend;
        var EventEmitter = core.EventEmitter;

        var BinaryData = (function () {

            function BinaryData(blob, name) {
                this.blob = blob;
                this.name = name;
                this.size = blob.size;
                this.type = blob.type;
            }

            return BinaryData;
        })();

        var Model = core.Model = (function () {

            extend(Model, EventEmitter);

            function Model(data) {

                var self = this;

                EventEmitter.call(self);

                self._data = {};
                self._deepComparisons = {};

                if (data) {

                    _.each(data, function (value, key) {

                        if (value !== undefined) {

                            self._data[key] = value;
                        }
                    });
                }
            }

            Model.prototype.set = function (key, value) {

                if (value === undefined) {

                    return;
                }

                var self = this;
                var prevValue = self._data[key];

                if (self._deepComparisons[key]) {

                    if (!_.isEqual(prevValue, value)) {

                        self._data[key] = value;
                        self.emit(key, value, prevValue);
                    }
                } else {

                    if (prevValue !== value) {

                        self._data[key] = value;
                        self.emit(key, value, prevValue);
                    }
                }
            };
            Model.prototype.get = function (key) {

                return this._data[key];
            };
            Model.prototype.remove = function (key) {

                var self = this;

                if (self._data[key] !== undefined) {

                    var prevValue = self._data[key];

                    delete self._data[key];
                    delete self._deepComparisons[key];

                    self.emit(key, undefined, prevValue);
                }
            };
            Model.prototype.change = function (key, listener, deepComparison) {

                var self = this;

                self._deepComparisons[key] = !!deepComparison;

                return self.on(key, listener);
            };

            return Model;
        })();

        var Switcher = core.Switcher = (function () {

            extend(Switcher, EventEmitter);

            function Switcher(states, value) {

                var self = this;

                EventEmitter.call(self);

                self.states = states;
                self.value = value || states[0].value;

                _.each(states, function (state) {

                    $(state.button).click(function () {

                        self.switchTo(state.value);
                    });

                    if (self.value !== state.value) {

                        $(state.button).removeClass('active');
                    } else {

                        $(state.button).addClass('active');
                    }
                });
            }

            Switcher.prototype.switchTo = function (value) {

                var self = this;

                if (value === self.value) {

                    return;
                }

                self.value = value;

                _.each(self.states, function (state) {

                    if (self.value !== state.value) {

                        $(state.button).removeClass('active');
                    } else {

                        $(state.button).addClass('active');
                    }
                });

                self.emit('change', value);
            };

            Switcher.prototype.clear = function () {

                var self = this;

                self.switchTo(self.states[0].value);
            };

            return Switcher;
        })();

        var GenderSwitcher = core.GenderSwitcher = (function () {

            extend(GenderSwitcher, Switcher);

            function GenderSwitcher(femaleButton, maleButton, gender) {
                Switcher.call(this, [{
                    value: 'female',
                    button: femaleButton
                }, {
                    value: 'male',
                    button: maleButton
                }], gender || 'female');
            }

            return GenderSwitcher;
        })();

        var Photo = core.Photo = (function () {

            var MIN_PHOTO_SIZE = 51200;
            var MAX_PHOTO_SIZE = 50485760;

            function getOrientation(blob) {

                var defer = $.Deferred();
                var reader = new FileReader();

                reader.addEventListener('load', function () {

                    var view = new DataView(reader.result);

                    if (view.getUint16(0, false) !== 0xFFD8) {

                        return defer.resolve(-2);
                    }

                    var length = view.byteLength;
                    var offset = 2;

                    while (offset < length) {

                        var marker = view.getUint16(offset, false);
                        offset += 2;

                        if (marker === 0xFFE1) {

                            if (view.getUint32(offset += 2, false) !== 0x45786966) {

                                return defer.resolve(-1);
                            }

                            var little = view.getUint16(offset += 6, false) === 0x4949;
                            offset += view.getUint32(offset + 4, little);

                            var tags = view.getUint16(offset, little);
                            offset += 2;

                            for (var i = 0; i < tags; i++) {

                                if (view.getUint16(offset + (i * 12), little) === 0x0112) {

                                    return defer.resolve(view.getUint16(offset + (i * 12) + 8, little));
                                }
                            }
                        }
                        else {

                            if ((marker & 0xFF00) !== 0xFF00) {

                                break;
                            } else {

                                offset += view.getUint16(offset, false);
                            }
                        }
                    }

                    return defer.resolve(-1);
                });

                reader.addEventListener('error', function (e) {

                    return defer.reject(e);
                });

                reader.readAsArrayBuffer(blob);

                return defer.promise();
            }

            function loadPhoto(blob, fixOrientation) {

                var defer = $.Deferred();
                var fileReader = new FileReader();

                fileReader.addEventListener('load', function () {

                    if (fixOrientation) {

                        getOrientation(blob).then(function (orientation) {

                            if (orientation > 1) {

                                var image = new Image();
                                image.addEventListener('load', function () {

                                    var canvas = document.createElement('canvas');
                                    var ctx = canvas.getContext('2d');
                                    var width = canvas.width = image.width;
                                    var height = canvas.height = image.height;

                                    switch (orientation) {
                                        case 2:
                                            ctx.translate(width, 0);
                                            ctx.scale(-1, 1);
                                            break;
                                        case 3:
                                            ctx.translate(width, height);
                                            ctx.rotate(180 / 180 * Math.PI);
                                            break;
                                        case 4:
                                            ctx.translate(0, height);
                                            ctx.scale(1, -1);
                                            break;
                                        case 5:
                                            canvas.width = height;
                                            canvas.height = width;
                                            ctx.rotate(90 / 180 * Math.PI);
                                            ctx.scale(1, -1);
                                            break;
                                        case 6:
                                            canvas.width = height;
                                            canvas.height = width;
                                            ctx.rotate(90 / 180 * Math.PI);
                                            ctx.translate(0, -height);
                                            break;
                                        case 7:
                                            canvas.width = height;
                                            canvas.height = width;
                                            ctx.rotate(270 / 180 * Math.PI);
                                            ctx.translate(-width, height);
                                            ctx.scale(1, -1);
                                            break;
                                        case 8:
                                            canvas.width = height;
                                            canvas.height = width;
                                            ctx.translate(0, width);
                                            ctx.rotate(270 / 180 * Math.PI);
                                            break;
                                    }

                                    ctx.drawImage(image, 0, 0, width, height);
                                    defer.resolve(
                                        canvas.toDataURL('image/jpeg', 0.95)
                                    );
                                });

                                image.src = fileReader.result;
                                return;
                            }

                            defer.resolve(fileReader.result);
                        }, function (e) {

                            defer.reject(e);
                        });
                    } else {

                        defer.resolve(fileReader.result);
                    }
                });

                fileReader.addEventListener('error', function (e) {

                    defer.reject(e);
                });

                fileReader.readAsDataURL(blob);

                return defer.promise();
            }

            function dataURL2Blob(dataURL) {

                var byteString = atob(dataURL.split(',')[1]);
                var mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0];
                var ab = new ArrayBuffer(byteString.length);
                var ia = new Uint8Array(ab);

                for (var i = 0; i < byteString.length; i++) {

                    ia[i] = byteString.charCodeAt(i);
                }

                return new Blob([ab], {type: mimeString});
            }

            extend(Photo, EventEmitter);

            function Photo(photoFrame, photoWrapper, dragOverlay, invalidPhotoOverlay, photo) {

                var self = this;

                EventEmitter.call(self);

                self.photoFrame = $(photoFrame);
                self.photoWrapper = $(photoWrapper);
                self.dragOverlay = $(dragOverlay);
                self.invalidPhotoOverlay = $(invalidPhotoOverlay);
                self.photo = $(photo);
                self.fileInput = $('<input type="file" style="display: none;">');

                self.binaryData = undefined;
                self.photoHeight = self.photoWrapper.height();
                self.defaultPhoto = self.photo.attr('src');

                self.photoFrame.append(self.fileInput);

                (function (listener) {

                    self.dragOverlay.on('dragover', listener);
                    self.dragOverlay.on('dragover', '*', listener);

                })(function (event) {

                    event.stopPropagation();
                    event.preventDefault();

                    event.originalEvent.dataTransfer.dropEffect = 'copy';
                });

                (function (listener) {

                    self.dragOverlay.on('drop', listener);
                    self.dragOverlay.on('drop', '*', listener);

                })(function (event) {

                    event.stopPropagation();
                    event.preventDefault();

                    self.dragOverlay.addClass('hidden');

                    var file = event.originalEvent.dataTransfer.files[0];
                    if (file) {

                        self.emit('change', self.fromBlob(file, file.name));
                    }
                });

                (function (listener) {

                    self.dragOverlay.on('dragleave', listener);
                    self.dragOverlay.on('dragleave', '*', listener);

                })(function (event) {

                    event.stopPropagation();
                    event.preventDefault();

                    self.dragOverlay.addClass('hidden');
                });

                _.each([self.invalidPhotoOverlay, self.photoWrapper], function (element) {

                    element.on('dragenter', function (event) {

                        event.stopPropagation();
                        event.preventDefault();

                        self.dragOverlay.removeClass('hidden');
                    });

                    element.click(function (e) {

                        e.stopPropagation();
                        self.fileInput.click();
                    });
                });

                self.fileInput.change(function () {

                    var file = this.files[0];
                    if (file) {

                        self.emit('change', self.fromBlob(file, file.name));
                        $(this).val('');
                    }
                });
            }

            Photo.prototype.isValid = function () {
                var binaryData = this.binaryData;
                return !!(binaryData && (binaryData.size >= MIN_PHOTO_SIZE && binaryData.size <= MAX_PHOTO_SIZE));
            };
            Photo.prototype.fromURL = function (url, name) {

                var self = this;
                var defer = $.Deferred();
                var xhr = new XMLHttpRequest();

                xhr.open('GET', url, true);
                xhr.responseType = 'blob';
                xhr.addEventListener('load', function () {

                    if (xhr.status < 200 || xhr.status >= 300) {

                        defer.reject('Unable to resolve URL');
                        return;
                    }

                    self.fromBlob(xhr.response, name + '.jpg')
                        .then(function () {

                            defer.resolve();
                        }, function (e) {

                            defer.reject(e);
                        });
                });

                xhr.send();

                return defer.promise();
            };
            Photo.prototype.fromBlob = function (blob, name) {

                var self = this;
                var defer = $.Deferred();

                if (!blob || !blob.type.match(/image.*/)) {

                    defer.reject('Invalid photo format');

                    return defer.promise();
                }

                console.log('before transform:' + blob.size);

                loadPhoto(blob, true).then(function (dataURL) {

                    var image = new Image();
                    var blob = dataURL2Blob(dataURL);
                    var objectURL = URL.createObjectURL(blob);

                    console.log('after transform:' + blob.size);

                    $(image).on('load', function () {

                        self.photo.removeClass('hidden');
                        self.photo.attr('src', objectURL);
                        self.photo.css('width', ((self.photoHeight / image.height) * image.width) + 'px');
                        self.photoFrame.addClass('has-photo');

                        if (self.isValid()) {

                            self.photoFrame.removeClass('has-error');
                        } else {

                            self.photoFrame.addClass('has-error');
                        }

                        self.emit('load');
                        defer.resolve();

                        setTimeout(function () {

                            URL.revokeObjectURL(objectURL);
                        }, 0);
                    });

                    image.src = objectURL;
                    self.binaryData = new BinaryData(blob, name);
                }, function () {

                    defer.reject('Unable to load photo');
                });

                return defer.promise();
            };
            Photo.prototype.clear = function (resetSource) {

                var self = this;

                if (resetSource) {

                    if (self.defaultPhoto) {

                        self.photo.attr('src', self.defaultPhoto);
                    } else {

                        self.photo.removeAttr('src');
                    }
                }

                self.photo.css('width', '');
                self.photoFrame.removeClass('has-photo');
                self.photoFrame.removeClass('has-error');
                self.binaryData = undefined;
            };

            Photo.MIN_PHOTO_SIZE = MIN_PHOTO_SIZE;
            Photo.MAX_PHOTO_SIZE = MAX_PHOTO_SIZE;

            return Photo;
        })();

        var PhotoGroup = core.PhotoGroup = (function () {

            extend(PhotoGroup, EventEmitter);

            function PhotoGroup(photosWrapper, frontPhoto, sidePhoto) {

                var self = this;

                EventEmitter.call(self);

                self.photosWrapper = $(photosWrapper);
                self.frontPhoto = frontPhoto;
                self.sidePhoto = sidePhoto;

                _.each([frontPhoto, sidePhoto], function (photo) {

                    photo.on('load', function () {

                        var hasError = false;

                        if (frontPhoto.binaryData && !frontPhoto.isValid()) {

                            hasError = true;
                        }

                        if (sidePhoto.binaryData && !sidePhoto.isValid()) {

                            hasError = true;
                        }

                        if (hasError) {

                            self.photosWrapper.addClass('has-error');
                        } else {

                            self.photosWrapper.removeClass('has-error');
                        }
                    });
                });
            }

            PhotoGroup.prototype.clear = function (resetSource) {

                var self = this;

                self.frontPhoto.clear(resetSource);
                self.sidePhoto.clear(resetSource);
                self.photosWrapper.removeClass('has-error');
            };

            return PhotoGroup;
        })();

        var HeightInput = core.HeightInput = (function () {

            var MIN_HEIGHT = 150;
            var MAX_HEIGHT = 220;

            extend(HeightInput, EventEmitter);

            function HeightInput(heightInputWrapper, cmInput, ftInput, inInput, cmUnitButton, inUnitButton, unit, height) {

                var self = this;

                EventEmitter.call(self);

                self.heightInputWrapper = $(heightInputWrapper);
                self.cmInput = $(cmInput);
                self.ftInput = $(ftInput);
                self.inInput = $(inInput);
                self.cmUnitButton = $(cmUnitButton);
                self.inUnitButton = $(inUnitButton);
                self.unit = self.defaultUnit = unit || 'cm';
                self.value = height || 0;

                self.heightInputWrapper.attr('data-unit', self.unit);
                self.recalculate();

                self.ftInput.keydown(function (e) {

                    if (e.keyCode === 13) {

                        self.inInput.focus();
                    }
                });

                _.each([self.cmInput, self.ftInput, self.inInput], function (input) {

                    input.on('input', function () {

                        var value = this.value;
                        if (value) {

                            var maxLength = $(this).attr('maxlength');

                            if (value.length > maxLength) {

                                value = value.substring(0, maxLength);
                            }

                            value = value.replace(/[^0-9]/g, '');
                            self.value = +value;

                            this.value = +value;
                        }

                        self.recalculate();
                        self.emit('change', self.value);
                    });

                    if (common.isMobileDevice()) {

                        input.attr('type', 'number');
                    }
                });

                self.unitSwitcher = new Switcher(
                    [{
                        value: 'cm',
                        button: $(cmUnitButton)
                    }, {
                        value: 'in',
                        button: $(inUnitButton)
                    }], self.unit
                );

                self.unitSwitcher.on('change', function (unit) {

                    self.setUnit(unit);
                    self.recalculate();
                });
            }

            HeightInput.prototype.focus = function () {

                var self = this;

                if (self.unit === 'cm') {

                    self.cmInput.focus();
                } else {

                    if (!self.ftInput.val()) {

                        self.ftInput.focus();
                    } else {

                        self.inInput.focus();
                    }
                }
            };
            HeightInput.prototype.blur = function () {

                var self = this;

                _.each([self.cmInput, self.ftInput, self.inInput], function (input) {

                    input.blur();
                });
            };
            HeightInput.prototype.isValid = function () {

                var self = this;

                return self.value >= MIN_HEIGHT && self.value <= MAX_HEIGHT;
            };
            HeightInput.prototype.recalculate = function () {

                var self = this;

                if (self.unit === 'cm') {

                    var cmValue = self.cmInput.val();
                    if (cmValue) {

                        self.value = +cmValue;
                    } else {

                        self.value = 0;
                        self.heightInputWrapper.removeClass('has-error');

                        return;
                    }
                } else {

                    var ftValue = self.ftInput.val();
                    var inValue = self.inInput.val();

                    if (ftValue || inValue) {

                        self.value = 0;
                        self.value += (+ftValue) * 30.48;
                        self.value += (+inValue) * 2.54;

                        self.value = Math.round(self.value);
                    } else {

                        self.value = 0;
                        self.heightInputWrapper.removeClass('has-error');

                        return;
                    }
                }

                if (self.isValid()) {

                    self.heightInputWrapper.removeClass('has-error');
                } else {

                    self.heightInputWrapper.addClass('has-error');
                }
            };
            HeightInput.prototype.setUnit = function (unit, value) {

                var self = this;

                if (unit === self.unit && !_.isNumber(value)) {

                    return;
                }

                if (_.indexOf(['cm', 'in'], unit) === -1) {

                    return;
                }

                self.heightInputWrapper.attr('data-unit', unit);
                self.unit = unit;

                if (_.isNumber(value)) {

                    switch (unit) {
                        case 'cm': {

                            self.value = value;
                            self.cmInput.val(value || '');
                            break;
                        }
                        case 'in': {

                            if (value) {

                                var feet = Math.floor(value / 30.48);
                                var inches = Math.round((value - (feet * 30.48)) / 2.54);

                                self.value = value;
                                self.ftInput.val(feet || 0);
                                self.inInput.val(inches || 0);
                            } else {

                                self.value = 0;
                                self.ftInput.val('');
                                self.inInput.val('');
                            }
                            break;
                        }
                    }

                    self.unitSwitcher.switchTo(unit);
                }

                self.recalculate();
                self.emit('change', self.value);
                self.emit('unit:change', unit);
            };
            HeightInput.prototype.clear = function () {

                var self = this;

                self.heightInputWrapper.attr('data-unit', self.defaultUnit);
                self.unit = self.defaultUnit;
                self.cmInput.val('');
                self.ftInput.val('');
                self.inInput.val('');
                self.unitSwitcher.switchTo(self.defaultUnit);
                self.emit('change', self.defaultUnit);
                self.emit('unit:change', self.defaultUnit);
            };

            return HeightInput;
        })();

        var Uploader = core.Uploader = (function () {

            function getEmptyData() {
                return {
                    frontName: undefined,
                    sideName: undefined,
                    key: undefined
                };
            }

            function Uploader(apiKey, apiEndpoint, model) {

                if (typeof apiKey === 'string') {

                    apiKey = $.when(apiKey);
                }

                var self = this;

                self.apiKey = apiKey;
                self.apiEndpoint = apiEndpoint;
                self.model = model;
                self._data = getEmptyData();

                _.each(['height', 'gender'], function (key) {

                    model.change(key, function () {

                        self._data = getEmptyData();
                    });
                });

                _.each(['frontPhoto', 'sidePhoto'], function (key) {

                    model.change(key, function () {

                        self._data = getEmptyData();
                    }, true);
                });
            }

            Uploader.prototype.upload = function () {

                var self = this;
                var defer = $.Deferred();

                self.apiKey.then(function (apiKey) {

                    self._step1(apiKey, function (error) {

                        if (error) {

                            defer.reject(error);
                        } else {

                            self._step2(apiKey, function (error) {

                                if (error) {

                                    defer.reject(error);
                                } else {

                                    self._complete(apiKey, function (error, response) {

                                        if (error) {

                                            defer.reject(error);
                                        } else {

                                            defer.resolve(response);
                                        }
                                    });
                                }
                            });
                        }
                    });
                });

                return defer.promise();
            };
            Uploader.prototype._post = function (apiKey, url, params, callback) {

                var formData = new FormData();
                var xhr = new XMLHttpRequest();

                _.each(params, function (value, key) {

                    if (value !== undefined) {

                        if (value instanceof BinaryData) {

                            formData.append(key, value.blob, value.name);
                        } else {

                            formData.append(key, value);
                        }
                    }
                });

                xhr.addEventListener('load', function () {
                    switch (xhr.status) {
                        case 200: {
                            callback(null, JSON.parse(xhr.responseText));
                            break;
                        }
                        case 400: {
                            callback('Bad request');
                            break;
                        }
                        case 500: {
                            callback('Oops... Something has broken, check your parameters and try again.');
                            break;
                        }
                        default: {
                            callback((function () {

                                try {

                                    return JSON.parse(xhr.responseText);
                                } catch (e) {

                                    return xhr.response;
                                }
                            })());
                        }
                    }
                });

                xhr.open('POST', url, true);

                if (apiKey) {

                    xhr.setRequestHeader('Authorization', 'APIKey ' + apiKey);
                }

                xhr.send(formData);
            };
            Uploader.prototype._uploadPhoto = function (apiKey, binaryData, callback) {

                var self = this;

                self._post(apiKey, self.apiEndpoint + '/uploads/', {
                    image: binaryData
                }, function (error, response) {

                    if (error || !response || !response.status) {

                        callback(error || response ? response.msg : 'Unknown error');
                    } else {

                        callback(null, response.name);
                    }
                });
            };
            Uploader.prototype._step1 = function (apiKey, callback) {

                var self = this;
                var data = self._data;

                if (data.frontName) {

                    callback();
                } else {

                    var model = self.model;

                    self._uploadPhoto(apiKey, model.get('frontPhoto'), function (error, name) {

                        if (error) {

                            callback(error);
                        } else {

                            self._post(apiKey, self.apiEndpoint + '/step/', {
                                image: name,
                                height: model.get('height'),
                                gender: model.get('gender'),
                                step: 1,
                                angle: 0
                            }, function (error, response) {

                                if (error || !response.status) {

                                    callback(error || response.error.message);
                                } else {

                                    data.frontName = name;
                                    data.key = response.key;

                                    callback();
                                }
                            });
                        }
                    });
                }
            };
            Uploader.prototype._step2 = function (apiKey, callback) {

                var self = this;
                var data = self._data;

                if (data.sideName) {

                    callback();
                } else {

                    var model = self.model;

                    self._uploadPhoto(apiKey, model.get('sidePhoto'), function (error, name) {

                        if (error) {

                            callback(error);
                        } else {

                            self._post(apiKey, self.apiEndpoint + '/step/', {
                                image: name,
                                height: model.get('height'),
                                gender: model.get('gender'),
                                step: 2,
                                angle: 0,
                                key: data.key
                            }, function (error, response) {

                                if (error || !response.status) {

                                    callback(error || response.error.message);
                                } else {

                                    data.sideName = name;

                                    callback();
                                }
                            });
                        }
                    });
                }
            };
            Uploader.prototype._complete = function (apiKey, callback) {

                var self = this;
                var data = self._data;
                var model = self.model;

                // if (model.get('gender') === 'male') {
                //     return self._post(apiKey, self.apiEndpoint + '/complete/?backend=humanslice', {
                //         image_1: data.frontName,
                //         image_2: data.sideName,
                //         height: model.get('height'),
                //         gender: model.get('gender'),
                //         angle: 0,
                //         key: data.key
                //     }, function (error, response) {
    
                //         if (error || !response.status) {
    
                //             callback(error || response.error.message);
                //         } else {
    
                //             callback(null, response);
                //         }
                //     });
                // }
                self._post(apiKey, self.apiEndpoint + '/complete/', {
                    image_1: data.frontName,
                    image_2: data.sideName,
                    height: model.get('height'),
                    gender: model.get('gender'),
                    angle: 0,
                    key: data.key
                }, function (error, response) {

                    if (error || !response.status) {

                        callback(error || response.error.message);
                    } else {

                        callback(null, response);
                    }
                });
            };

            return Uploader;
        })();

        var Uploader2 = core.Uploader2 = (function () {

            function getEmptyData() {
                return {
                    frontName: undefined,
                    sideName: undefined,
                    key: undefined
                };
            }

            function Uploader2(apiKey, apiEndpoint, model) {
                console.log(model);

                if (typeof apiKey === 'string') {

                    apiKey = $.when(apiKey);
                }

                var self = this;

                self.apiKey = apiKey;
                self.apiEndpoint = apiEndpoint;
                self.model = model;
                self._data = getEmptyData();

                _.each(['height', 'gender'], function (key) {

                    model.change(key, function () {

                        self._data = getEmptyData();
                    });
                });

                _.each(['frontPhoto', 'sidePhoto'], function (key) {

                    model.change(key, function () {

                        self._data = getEmptyData();
                    }, true);
                });
            }

            Uploader2.prototype.upload = function () {

                var self = this;
                var model = self.model;
                var defer = $.Deferred();

                self.apiKey.then(function (apiKey) {

                    self._createInstance(apiKey, {
                        height: model.get('height'),
                        gender: model.get('gender'),
                    }, function (err, response) {
                        if (err) {
                            return defer.reject(err);
                        }
                        var instanceUrl = response.location;
                        self._uploadPhotos(response.location, apiKey, {
                            front_image: model.get('frontPhoto'),
                            side_image: model.get('sidePhoto'),
                        }, function (err, response) {
                            if (err) {
                                return defer.reject(err);
                            }

                            self._getResult(response.location, apiKey, function (err, response) {
                                // try to handle redirect error
                                if (err === 'Redirect') {
                                    return self._get(apiKey, instanceUrl, function (err, response) {
                                        if (err) {
                                            return defer.reject(err);
                                        }
        
                                        return defer.resolve(response);
                                    });
                                }

                                if (err) {
                                    return defer.reject(err);
                                }

                                return defer.resolve(response);
                            });
                        });
                    });
                });

                return defer.promise();
            };
            Uploader2.prototype._post = function (apiKey, url, params, callback) {

                var formData = new FormData();
                var xhr = new XMLHttpRequest();

                _.each(params, function (value, key) {

                    if (value !== undefined) {

                        if (value instanceof BinaryData) {

                            formData.append(key, value.blob, value.name);
                        } else {

                            formData.append(key, value);
                        }
                    }
                });

                xhr.addEventListener('load', function () {
                    var response = {
                        body: JSON.parse(xhr.responseText),
                        location: xhr.getResponseHeader('location'),
                    };
                    switch (xhr.status) {
                        case 200: {
                            callback(null, response);
                            break;
                        }
                        case 201: {
                            callback(null, response);
                            break;
                        }
                        case 400: {
                            callback('Bad request');
                            break;
                        }
                        case 500: {
                            callback('Oops... Something has broken, check your parameters and try again.');
                            break;
                        }
                        default: {
                            callback((function () {

                                try {

                                    return response;
                                } catch (e) {

                                    return xhr.response;
                                }
                            })());
                        }
                    }
                });

                xhr.open('POST', url, true);

                if (apiKey) {

                    xhr.setRequestHeader('Authorization', 'APIKey ' + apiKey);
                }

                xhr.send(formData);
            };
            Uploader2.prototype._patch = function (apiKey, url, params, callback) {

                var formData = new FormData();
                var xhr = new XMLHttpRequest();

                _.each(params, function (value, key) {

                    if (value !== undefined) {

                        if (value instanceof BinaryData) {

                            formData.append(key, value.blob, value.name);
                        } else {

                            formData.append(key, value);
                        }
                    }
                });

                xhr.addEventListener('load', function () {
                    var response = {
                        body: (xhr.responseText) ? JSON.parse(xhr.responseText) : xhr.responseText,
                        location: xhr.getResponseHeader('location'),
                    };
                    switch (xhr.status) {
                        case 200: {
                            callback(null, response);
                            break;
                        }
                        case 202: {
                            callback(null, response);
                            break;
                        }
                        case 400: {
                            callback('Bad request');
                            break;
                        }
                        case 500: {
                            callback('Oops... Something has broken, check your parameters and try again.');
                            break;
                        }
                        default: {
                            callback((function () {

                                try {

                                    return response;
                                } catch (e) {

                                    return xhr.response;
                                }
                            })());
                        }
                    }
                });

                xhr.open('PUT', url, true);

                if (apiKey) {

                    xhr.setRequestHeader('Authorization', 'APIKey ' + apiKey);
                }

                xhr.send(formData);
            };
            Uploader2.prototype._get = function (apiKey, url, callback) {
                var xhr = new XMLHttpRequest();

                xhr.addEventListener('load', function () {
                    var response = {
                        body: (xhr.responseText) ? JSON.parse(xhr.responseText) : xhr.responseText,
                        location: xhr.getResponseHeader('location'),
                    };
                    switch (xhr.status) {
                        case 200: {
                            callback(null, response);
                            break;
                        }
                        case 202: {
                            callback(null, response);
                            break;
                        }
                        case 400: {
                            callback('Bad request');
                            break;
                        }
                        case 401: {
                            callback('Redirect');
                            break;
                        }
                        case 500: {
                            callback('Oops... Something has broken, check your parameters and try again.');
                            break;
                        }
                        default: {
                            callback((function () {

                                try {

                                    return response;
                                } catch (e) {

                                    return xhr.response;
                                }
                            })());
                        }
                    }
                });

                xhr.addEventListener('error', function () {
                    callback('Redirect');
                });

                xhr.open('GET', url, true);

                if (apiKey) {

                    xhr.setRequestHeader('Authorization', 'APIKey ' + apiKey);
                }

                xhr.send();
            };
            Uploader2.prototype._createInstance = function (apiKey, data, callback) {

                var self = this;

                self._post(apiKey, self.apiEndpoint, data, function (error, response) {

                    if (error) {
                        return callback(error);
                    }
                    return callback(null, response);
                });
            };
            Uploader2.prototype._uploadPhotos = function (url, apiKey, data, callback) {

                var self = this;

                self._patch(apiKey, url, data, function (error, response) {

                    if (error) {
                        return callback(error);
                    }
                    return callback(null, response);
                });
            };
            Uploader2.prototype._getResult = function (url, apiKey, callback) {

                var self = this;
                var timer;

                timer = setInterval(function () {
                    self._get(apiKey, url, function (error, response) {
                        if (error) {
                            clearInterval(timer);
                            return callback(error);
                        }

                        console.log(response);

                        if (response.body.is_ready === true && response.body.is_successful === false) {
                            clearInterval(timer);
                            return callback(new Error('Error'));
                        }
    
                        if (response.body.is_successful === undefined) {
                            clearInterval(timer);
                            return callback(null, response.body);
                        }
                    });
                }, 2000);
            };

            return Uploader2;
        })();

    })(saia.core || (saia.core = {}));

})(window.saia || (window.saia = {}));
