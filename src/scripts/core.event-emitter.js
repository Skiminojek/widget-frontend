'use strict';

(function (saia) {

    (function (core) {

        var EventEmitter = core.EventEmitter = (function () {

            function EventEmitter() {
                this._listeners = {};
            }

            EventEmitter.prototype.on = function (name, listener) {

                var self = this;
                var listeners = this._listeners;

                if (!listeners[name]) {

                    listeners[name] = [];
                }

                listeners[name].push(listener);

                return function () {

                    var index = _.indexOf(listeners[name], listener);

                    if (index !== -1) {

                        listeners[name].splice(index, 1);

                        if (listeners[name].length === 0) {

                            delete listeners[name];
                        }
                    }
                };
            };
            EventEmitter.prototype.emit = function () {

                var self = this;
                var name = arguments[0];
                var args = _.slice(arguments, 1, arguments.length);

                _.each(self._listeners[name] || [], function (listener) {

                    listener.apply(self, args);
                });
            };

            return EventEmitter;
        })();

    })(saia.core || (saia.core = {}));

})(window.saia || (window.saia = {}));