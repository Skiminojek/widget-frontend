'use strict';

(function (env, saia) {

    var DEVELOPMENT = false;
    var UPDATE_INTERVAL = 3000;
    var FIELDS = {
        'volume.chest': 'Chest',
        'volume.waist': 'Waist',
        'volume.hips': 'Hips',
        'volume.neck': 'Neck',
        'front.shoulders': 'Shoulders width',
        'front.chest': 'Front chest width',
        'front.waist': 'Front waist width',
        'front.hips': 'Front hips width',
        'front.shoulder_to_waist': 'Shoulders to waist length',
        'front.sleeve_length': 'Sleeve length',
        'front.body_height': 'Body length',
        'front.legs_height': 'Legs length',
        'front.outside_legs_length': 'Outside leg length',
        'front.inside_legs_length': 'Inside leg length',
        'front.hip_height': 'Waist-to-hip length',
        'front.crotch_length': 'Urise (Crotch) length',
        'front.hem': 'Hem width',
        'front.forearm': 'Forearm',
        'front.jacket_lenght': 'Jacket Lenght',
        'front.nape_to_waist_centre_back': 'Nape to Waist Centre Back',
        'front.scye_depth': 'Scye Depth',
        'front.shoulder_length': 'Shoulder Length',
        'side.chest': 'Side chest width',
        'side.waist': 'Side waist width',
        'side.hips': 'Side hips width',
        'side.inseam': 'Inseam',
        'side.outseam': 'Outseam',
        'side.nape_to_bust': 'Nape to Bust',
        'side.nape_to_waist_over_bust': 'Nape to Waist over Bust'
    };

    var common = saia.common;
    var modelViewerContainerElement = $('.model-viewer .container');
    var parametersElement = $('.model-viewer .parameters-wrapper .parameters');
    var unitCm = $('.model-viewer .parameters-wrapper .unit-switcher .cm');
    var unitInch = $('.model-viewer .parameters-wrapper .unit-switcher .inch');

    var modelViewer = new saia.ModelViewer();
    var currentModelURL = null;
    var currentParameters = null;
    var currentUnit = null;

    function showLoader(label) {

        $('.model-viewer .loader')
            .text(label || '')
            .removeClass('hidden');
    }

    function hideLoader() {

        $('.model-viewer .loader')
            .addClass('hidden');
    }

    function formatParameter(value, unit) {

        switch (unit) {
            case 'cm':
                return Math.round(+value) + ' cm';
            case 'inch':
                return +value ? +(+value / 2.54).toFixed(2) + '\'' : '0.00\'';
        }
    }

    function updateParameters() {

        if (currentParameters) {

            parametersElement.empty();

            _.each(FIELDS, function (name, path) {

                var value = _.get(currentParameters, path);
                if (_.isUndefined(value)) {

                    return;
                }

                $('' +
                    '<div class="parameter">' +
                    '   <div class="parameter-name">' + name + '</div>' +
                    '   <div class="parameter-value">' + formatParameter(value, currentUnit) + '</div>' +
                    '</div>' +
                    ''
                ).appendTo(parametersElement);
            });
        }
    }

    function switchUnit(unit) {

        if (currentUnit !== unit) {

            switch (unit) {
                case 'cm': {
                    unitCm.addClass('active');
                    unitInch.removeClass('active');
                    break;
                }
                case 'inch': {
                    unitInch.addClass('active');
                    unitCm.removeClass('active');
                    break;
                }
            }

            currentUnit = unit;
            updateParameters();
        }
    }

    unitCm.click(function () {

        switchUnit('cm');
    });

    unitInch.click(function () {

        switchUnit('inch');
    });

    $(window).on('load', function () {

        $('.model-creator').addClass('loaded');

        modelViewer.init(modelViewerContainerElement);

        function update() {

            $.ajax({
                type: 'GET',
                url: 'https://saia.3dlook.me/companies/get-model-url/',
                dataType: 'json'
            }).then(function (response) {

                var modelURL = _.get(response, 'model_3d_url');

                if (modelURL !== currentModelURL) {

                    if (currentModelURL) {

                        showLoader('Updating...');
                    } else {

                        showLoader('Loading...');
                    }

                    modelViewer.load(modelURL)
                        .then(function () {

                            parametersElement.scrollTop(0);
                            currentModelURL = modelURL;
                            currentParameters = _.get(response, 'parameters');

                            hideLoader();
                            updateParameters();
                            setTimeout(update, UPDATE_INTERVAL);
                        }, function () {

                            hideLoader();
                            setTimeout(update, UPDATE_INTERVAL);
                        });
                } else {

                    setTimeout(update, UPDATE_INTERVAL);
                }
            }, function () {

                setTimeout(update, UPDATE_INTERVAL);
            });
        }

        update();
    });

    $(window).on('resize', function () {

        if (common.isMobileDevice()) {

            modelViewerContainerElement.css({
                height: ($(window).height() / 2) + 'px'
            });
        }
    });

    if (common.isMobileDevice()) {

        $('.model-creator').addClass('mobile');
    }

    switchUnit('cm');

})('<Config:env>', window.saia);
