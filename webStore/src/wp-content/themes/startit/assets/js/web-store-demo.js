'use strict';

(function (env) {

    function resolveTemplates() {

        var templatePlaces = $('[data-x-template]');

        if (templatePlaces.length > 0) {

            templatePlaces.each(function () {

                $(this).replaceWith(
                    $('#' + $(this).data('x-template')).html()
                );
            });

            resolveTemplates();
        }
    }

    resolveTemplates();

    $(document).ready(function () {
        var demoContainer = $('.web-store-demo');
        var normalScreen = $('.screen.normal');

        function preventDefault(e) {
            e = e || window.event;
            if (e.preventDefault)
                e.preventDefault();
            e.returnValue = false;
        }

        $('.switch-view-button').click(function () {

            if (normalScreen.hasClass('desktop-view')) {

                normalScreen.removeClass('desktop-view');
                normalScreen.addClass('mobile-view');
            } else {

                normalScreen.removeClass('mobile-view');
                normalScreen.addClass('desktop-view');
            }
        });

        $('.person.woman').click(function () {

            demoContainer.addClass('for-woman');
            demoContainer.removeClass('for-man');
        });

        $('.person.man').click(function () {

            demoContainer.addClass('for-man');
            demoContainer.removeClass('for-woman');
        });

        $('.screen').addClass('loaded');

        const button1 = new SaiaButton({
            key: '20f3e93762b5f938b95320c59a16159106ebbae2',
            product: {
                url: 'https://3dlook.me/saia/',
            },
        });
        button1.init();

        const button2 = new SaiaButton({
            container: '.mobile .saia-widget-container',
            key: '20f3e93762b5f938b95320c59a16159106ebbae2',
            product: {
                url: 'https://3dlook.me/saia/',
            },
        });
        button2.init();

        const button3 = new SaiaButton({
            container: '.screen.small.loaded .saia-widget-container',
            key: '20f3e93762b5f938b95320c59a16159106ebbae2',
            product: {
                url: 'https://3dlook.me/saia/',
            },
        });
        button3.init();

        if (env === 'dev') {

            $('.photo').click(function () {

                location.reload(true);
            });
        }
    });

})('<Config:env>');