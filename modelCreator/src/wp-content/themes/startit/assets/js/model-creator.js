'use strict';

(function (env, saia) {

    var USAGE_LIMIT = 3;
    var USAGE_LIMIT_DELAY = 86400000;
    /*var USAGE_LIMIT_DELAY = 10 * 1000;*/

    var modal = $('.modal');
    var closeBtn = modal.find('.modal__close');

    closeBtn.on('click', function (e) {
        e.preventDefault();
        modal.hide();
    });

    var url = new URL(window.location.href);
    var isExistingUser = url.searchParams.get('existing-user');

    if (isExistingUser) {
        $('.model-creator-tip').show();
    } else {
        modal.show();
    }

    var DEVELOPMENT = false;
    var DEV_RESPONSE = {
        "points": {
            "front": {
                "hips": [
                    [886.427523136139, 311.1714137792587],
                    [885.4663696289064, 431.06039643287664]
                ],
                "front_shoulder_width": [
                    [722.097000360489, 447.77706527709967],
                    [723.3296613693238, 304.9271367788315]
                ],
                "waist": [
                    [823.5733952522279, 327.027197599411],
                    [823.7898101806642, 419.9387315511704]
                ],
                "points_expended": [
                    [933.84, 297.49],
                    [903.18, 293.1],
                    [885.0, 293.0],
                    [868.0, 294.0],
                    [850.0, 294.0],
                    [833.0, 295.0],
                    [815.89, 295.52],
                    [797.0, 297.0],
                    [778.0, 299.0],
                    [760.0, 301.0],
                    [741.0, 303.0],
                    [723.33, 304.93],
                    [716.0, 311.0],
                    [710.1, 317.8],
                    [704.0, 337.0],
                    [698.79, 356.23],
                    [684.0, 354.0],
                    [670.83, 353.61],
                    [658.0, 350.0],
                    [646.28, 347.42],
                    [627.0, 354.0],
                    [609.39, 361.97],
                    [609.0, 376.0],
                    [609.03, 390.86],
                    [627.0, 397.0],
                    [646.14, 403.86],
                    [658.0, 400.0],
                    [670.36, 397.93],
                    [683.0, 396.0],
                    [697.46, 395.48],
                    [703.0, 414.0],
                    [708.96, 433.86],
                    [715.0, 440.0],
                    [722.1, 447.78],
                    [740.0, 448.0],
                    [759.0, 449.0],
                    [778.0, 449.0],
                    [797.0, 450.0],
                    [816.28, 451.24],
                    [833.0, 450.0],
                    [851.0, 449.0],
                    [869.0, 449.0],
                    [887.0, 448.0],
                    [904.72, 447.84],
                    [936.53, 440.59],
                    [965.82, 422.56],
                    [927.0, 422.64],
                    [899.65, 431.46],
                    [883.0, 430.0],
                    [868.0, 428.0],
                    [852.0, 427.0],
                    [836.0, 425.0],
                    [820.71, 424.33],
                    [804.0, 424.0],
                    [787.0, 425.0],
                    [771.0, 425.0],
                    [755.0, 426.0],
                    [738.77, 427.01],
                    [763.59, 425.54],
                    [783.0, 423.0],
                    [803.0, 421.0],
                    [823.79, 419.94],
                    [844.0, 423.0],
                    [864.0, 427.0],
                    [885.47, 431.06],
                    [915.0, 426.0],
                    [945.0, 422.0],
                    [974.0, 418.0],
                    [1004.0, 414.0],
                    [1034.46, 410.55],
                    [1062.0, 407.0],
                    [1091.0, 404.0],
                    [1119.0, 401.0],
                    [1147.0, 399.0],
                    [1175.81, 396.22],
                    [1204.74, 407.63],
                    [1219.95, 394.17],
                    [1207.33, 374.82],
                    [1175.7, 373.97],
                    [1147.0, 373.0],
                    [1119.0, 373.0],
                    [1091.0, 373.0],
                    [1063.0, 373.0],
                    [1035.4, 372.96],
                    [1012.0, 372.0],
                    [989.0, 372.0],
                    [966.0, 372.0],
                    [943.0, 372.0],
                    [920.72, 371.77],
                    [943.0, 371.0],
                    [966.0, 370.0],
                    [989.0, 369.0],
                    [1012.0, 369.0],
                    [1035.06, 368.77],
                    [1063.0, 368.0],
                    [1091.0, 368.0],
                    [1120.0, 368.0],
                    [1148.0, 368.0],
                    [1177.33, 367.95],
                    [1207.24, 369.73],
                    [1220.95, 351.35],
                    [1206.47, 336.26],
                    [1176.51, 343.96],
                    [1147.0, 340.0],
                    [1119.0, 337.0],
                    [1090.0, 334.0],
                    [1062.0, 331.0],
                    [1033.75, 328.08],
                    [1004.0, 324.0],
                    [974.0, 321.0],
                    [945.0, 317.0],
                    [915.0, 314.0],
                    [886.43, 311.17],
                    [865.0, 316.0],
                    [844.0, 321.0],
                    [823.57, 327.03],
                    [803.0, 326.0],
                    [784.0, 325.0],
                    [764.27, 324.12],
                    [740.78, 323.51],
                    [757.0, 323.0],
                    [773.0, 323.0],
                    [789.0, 323.0],
                    [806.0, 322.0],
                    [822.55, 322.8],
                    [838.0, 320.0],
                    [853.0, 317.0],
                    [869.0, 315.0],
                    [884.0, 312.0],
                    [900.26, 309.94],
                    [925.54, 316.69],
                    [963.75, 316.78]
                ],
                "hip_height": [
                    [886.427523136139, 311.1714137792587],
                    [823.5733952522279, 327.027197599411]
                ],
                "crotch_lenght": [
                    [823.5733952522279, 327.027197599411],
                    [920.7194008827211, 371.7735832929612]
                ],
                "inside_legs_lenght": [
                    [1175.6977744102478, 373.97468698024755],
                    [920.7194008827211, 371.7735832929612],
                    [1177.3265933990479, 367.94976317882544]
                ],
                "shoulders": [
                    [723.3296613693238, 304.9271367788315],
                    [722.097000360489, 447.77706527709967]
                ],
                "shoulder_to_waist": [
                    [698.7921073436738, 356.2253572940827],
                    [823.5733952522279, 327.027197599411],
                    [697.4649131298066, 395.4814149141312],
                    [823.7898101806642, 419.9387315511704]
                ],
                "neck": [
                    [698.7921073436738, 356.2253572940827],
                    [697.4649131298066, 395.4814149141312]
                ],
                "all": [
                    [933.8383269309999, 297.4865598678589],
                    [903.1777560710908, 293.1035360097885],
                    [815.8888571262361, 295.5205178260803],
                    [723.3296613693238, 304.9271367788315],
                    [710.0954082012178, 317.8021298646927],
                    [698.7921073436738, 356.2253572940827],
                    [670.8286428451538, 353.6139410734177],
                    [646.2802398204803, 347.4155462980271],
                    [609.394829750061, 361.96823537349707],
                    [609.0303146839142, 390.8606736660004],
                    [646.1425366401672, 403.8569834232331],
                    [670.3582935333252, 397.92794895172125],
                    [697.4649131298066, 395.4814149141312],
                    [708.9580149650575, 433.85621738433844],
                    [722.097000360489, 447.77706527709967],
                    [816.2832069396974, 451.2441962957383],
                    [904.7182574272157, 447.8426678180695],
                    [936.5343031883241, 440.5852242708207],
                    [965.824453353882, 422.5600361824036],
                    [926.9952077865602, 422.6422512531281],
                    [899.6522834300996, 431.4573454856873],
                    [820.711237192154, 424.33370625972753],
                    [738.7691996097566, 427.0086060762406],
                    [763.5880365371705, 425.53811466693884],
                    [823.7898101806642, 419.9387315511704],
                    [885.4663696289064, 431.06039643287664],
                    [1034.4617757797241, 410.54688870906836],
                    [1175.8128190040588, 396.2246843576432],
                    [1204.7362504005432, 407.62873399257666],
                    [1219.9528756141663, 394.17286741733557],
                    [1207.333116531372, 374.816755771637],
                    [1175.6977744102478, 373.97468698024755],
                    [1035.4011182785034, 372.9607293605805],
                    [920.7194008827211, 371.7735832929612],
                    [1035.0556454658508, 368.7658113241196],
                    [1177.3265933990479, 367.94976317882544],
                    [1207.2406740188599, 369.73315215110784],
                    [1220.9530177116394, 351.34729135036474],
                    [1206.4693779945374, 336.2560511827469],
                    [1176.5050077438354, 343.9556479454041],
                    [1033.7527484893799, 328.0829408168793],
                    [886.427523136139, 311.1714137792587],
                    [823.5733952522279, 327.027197599411],
                    [764.2671725749971, 324.1217283010483],
                    [740.7836101055146, 323.51294124126434],
                    [822.5537023544313, 322.80083441734314],
                    [900.2588102817537, 309.94061744213104],
                    [925.5430240631105, 316.69298923015594],
                    [963.7534246444703, 316.77774703502655]
                ],
                "chest": [
                    [764.2671725749971, 324.1217283010483],
                    [763.5880365371705, 425.53811466693884]
                ],
                "legs_height": [
                    [886.427523136139, 311.1714137792587],
                    [1220.9530177116394, 351.34729135036474]
                ],
                "body_height": [
                    [886.427523136139, 311.1714137792587],
                    [697.4649131298066, 395.4814149141312]
                ],
                "chest_top": [
                    [740.7836101055146, 323.51294124126434],
                    [738.7691996097566, 427.0086060762406]
                ]
            },
            "side": {
                "all": [
                    [656.8580703735352, 317.9671815633774],
                    [685.2412555217744, 341.33730983734137],
                    [706.4008719921113, 350.23015522956854],
                    [735.6577970981599, 330.55606055259705],
                    [762.6940677165986, 314.65199303627014],
                    [782.9095952510835, 318.8682700395584],
                    [834.0944380760194, 321.7645572423935],
                    [877.031329393387, 325.9326070547104],
                    [1043.3588590621948, 362.1894249916077],
                    [1175.3892560005188, 371.88761079311377],
                    [1193.5745515823364, 342.8680075407029],
                    [1199.0476455688477, 316.7222024202347],
                    [1214.1171312332153, 317.89454412460327],
                    [1221.6483707427979, 345.0480630397797],
                    [1210.6891865730286, 410.31016016006475],
                    [1176.2196564674377, 408.8397817611695],
                    [1041.6405358314514, 406.7411502599717],
                    [936.6282148361207, 408.4609426259995],
                    [903.593351840973, 420.2104933261872],
                    [872.7048389911653, 409.4773582220078],
                    [831.1188740730287, 396.39414346218115],
                    [760.9319529533387, 405.9186605215073],
                    [715.9601962566377, 400.12990021705633],
                    [821.1613576412202, 393.17586135864263],
                    [911.0748667716981, 370.67636525630957],
                    [938.8737316131593, 370.8613915443421],
                    [971.2202482223512, 359.25951719284063],
                    [940.9189939498903, 341.464616060257],
                    [908.3389413356782, 346.8524436950684],
                    [816.6920220851899, 362.12774956226355],
                    [741.797991514206, 353.5112993717194],
                    [715.8987751007081, 400.00313079357153],
                    [693.7626922130586, 382.0619941949845],
                    [672.8072855472565, 379.5298830270768],
                    [632.5523571968079, 391.49895644187933],
                    [605.0790753364563, 355.2437489032746]
                ],
                "hips": [
                    [872.7048389911653, 409.4773582220078],
                    [877.031329393387, 325.9326070547104]
                ],
                "chest": [
                    [760.9319529533387, 405.9186605215073],
                    [762.6940677165986, 314.65199303627014]
                ],
                "waist": [
                    [831.1188740730287, 396.39414346218115],
                    [834.0944380760194, 321.7645572423935]
                ],
                "points_expended": [
                    [656.86, 317.97],
                    [662.0, 322.0],
                    [668.0, 327.0],
                    [673.0, 331.0],
                    [679.0, 336.0],
                    [685.24, 341.34],
                    [689.0, 343.0],
                    [693.0, 344.0],
                    [697.0, 346.0],
                    [702.0, 348.0],
                    [706.4, 350.23],
                    [735.66, 330.56],
                    [762.69, 314.65],
                    [782.91, 318.87],
                    [799.0, 319.0],
                    [817.0, 320.0],
                    [834.09, 321.76],
                    [848.0, 323.0],
                    [862.0, 324.0],
                    [877.03, 325.93],
                    [910.0, 333.0],
                    [943.0, 340.0],
                    [976.0, 347.0],
                    [1010.0, 354.0],
                    [1043.36, 362.19],
                    [1069.0, 364.0],
                    [1096.0, 366.0],
                    [1122.0, 368.0],
                    [1148.0, 369.0],
                    [1175.39, 371.89],
                    [1181.0, 362.0],
                    [1187.0, 352.0],
                    [1193.57, 342.87],
                    [1199.05, 316.72],
                    [1214.12, 317.89],
                    [1221.65, 345.05],
                    [1217.0, 366.0],
                    [1214.0, 388.0],
                    [1210.69, 410.31],
                    [1176.22, 408.84],
                    [1149.0, 408.0],
                    [1122.0, 408.0],
                    [1095.0, 407.0],
                    [1068.0, 407.0],
                    [1041.64, 406.74],
                    [1020.0, 407.0],
                    [999.0, 407.0],
                    [978.0, 407.0],
                    [957.0, 408.0],
                    [936.63, 408.46],
                    [903.59, 420.21],
                    [872.7, 409.48],
                    [864.0, 406.0],
                    [856.0, 404.0],
                    [847.0, 401.0],
                    [839.0, 399.0],
                    [831.12, 396.39],
                    [807.0, 399.0],
                    [784.0, 402.0],
                    [760.93, 405.92],
                    [715.96, 400.13],
                    [737.0, 398.0],
                    [758.0, 397.0],
                    [779.0, 395.0],
                    [800.0, 394.0],
                    [821.16, 393.18],
                    [839.0, 388.0],
                    [857.0, 384.0],
                    [875.0, 379.0],
                    [893.0, 375.0],
                    [911.07, 370.68],
                    [938.87, 370.86],
                    [971.22, 359.26],
                    [940.92, 341.46],
                    [908.34, 346.85],
                    [890.0, 349.0],
                    [871.0, 352.0],
                    [853.0, 356.0],
                    [835.0, 359.0],
                    [816.69, 362.13],
                    [801.0, 360.0],
                    [786.0, 358.0],
                    [771.0, 356.0],
                    [756.0, 355.0],
                    [741.8, 353.51],
                    [715.9, 400.0],
                    [693.76, 382.06],
                    [672.81, 379.53],
                    [659.0, 383.0],
                    [645.0, 387.0],
                    [632.55, 391.5],
                    [623.0, 379.0],
                    [614.0, 367.0],
                    [605.08, 355.24]
                ]
            }
        },
        "data_save": true,
        "cloth_sizes": [{
            "brand": "Asos",
            "size_num": "6",
            "body_part": "dress",
            "gender": "female",
            "size_alpha": null
        }, {
            "brand": "Lauren Conrad",
            "size_num": "2",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Vilonna",
            "size_num": "34",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Gap",
            "size_num": "000",
            "body_part": "dress",
            "gender": "female",
            "size_alpha": "XXS"
        }, {
            "brand": "Mr520",
            "size_num": "34",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Asos",
            "size_num": "2",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XXS"
        }, {
            "brand": "Gant",
            "size_num": "4",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Hollister",
            "size_num": "32",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Free People",
            "size_num": "00",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XXS"
        }, {
            "brand": "Madewell",
            "size_num": "0",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Ralph Lauren",
            "size_num": "2",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Ralph Lauren",
            "size_num": "6",
            "body_part": "bottom",
            "gender": "female",
            "size_alpha": "S"
        }, {
            "brand": "J.Crew",
            "size_num": "0",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "G-Star Raw",
            "size_num": "",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Levi Strauss",
            "size_num": "30",
            "body_part": "bottom",
            "gender": "female",
            "size_alpha": null
        }, {
            "brand": "Guess",
            "size_num": "0-2",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Banana Republic",
            "size_num": "0",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Ted Baker",
            "size_num": "0",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XXS"
        }, {
            "brand": "Urban Outfitters",
            "size_num": "",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Kate Spade",
            "size_num": "00",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XXS"
        }, {
            "brand": "Topshop",
            "size_num": "4",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "S"
        }, {
            "brand": "Forever21",
            "size_num": "0",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Asos",
            "size_num": "6",
            "body_part": "bottom",
            "gender": "female",
            "size_alpha": ""
        }, {
            "brand": "H&M",
            "size_num": "2-4",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Levi Strauss",
            "size_num": "",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XXS"
        }, {
            "brand": "Levi Strauss",
            "size_num": "27",
            "body_part": "bottom",
            "gender": "female",
            "size_alpha": "4/5"
        }, {
            "brand": "Tommy Hilfiger",
            "size_num": "",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Old Navy",
            "size_num": "0-2",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Zara",
            "size_num": "4",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Adidas",
            "size_num": "4-6",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Nike",
            "size_num": "0-2",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Gap",
            "size_num": "0",
            "body_part": "top",
            "gender": "female",
            "size_alpha": "XS"
        }, {
            "brand": "Gap",
            "size_num": "6",
            "body_part": "bottom",
            "gender": "female",
            "size_alpha": "S"
        }],
        "body_type": "Rectangle",
        "legs_height": 91.29544845728232,
        "side_parameters": {
            "nape_to_bust": 24.47361885376984,
            "hips": 22.62839481123336,
            "waist": 20.213690483063832,
            "outseam": 93.4717818244336,
            "height_p": 616.5692954063416,
            "height": 167.0,
            "nape_to_waist_over_bust": 38.250478883806935,
            "h": 0.27085357841236796,
            "inseam": 78.0249422960217,
            "chest": 24.7199034781482
        },
        "front_parameters": {
            "hips": 32.718936565174396,
            "nape_to_waist_centre_back": 37.69120343860973,
            "hip_height": 17.153538028049883,
            "crotch_length": 53.02428839466446,
            "height_p": 611.9227030277252,
            "neck_height": 7.51459620501166,
            "shoulders": 38.98521486647622,
            "inside_legs_length": 69.80848161589833,
            "waist": 25.356513319690976,
            "neck": 10.713381919172024,
            "outside_legs_length": 79.20183096290839,
            "chest": 27.677575025903817,
            "h": 0.27291028617454893,
            "body_height": 51.25758315740174,
            "forearm": 22.514280816285677,
            "shoulder_length": 7.652018657727233,
            "hem": 6.310247003253061,
            "neck_line": {
                "value": 10.713381919172024,
                "name": "long_and_thin"
            },
            "shoulder_to_waist": 32.7093872030759,
            "legs_height": 91.29544845728232,
            "shoulders_line": {
                "right": 16.15658760565831,
                "left": 15.969373372232173
            },
            "jacket_lenght": 62.64117840050315,
            "scye_depth": 14.00665473536019,
            "height": 167.0,
            "sleeve_length": 49.54805716041311,
            "chest_top": 28.24503150793857
        },
        "message": "",
        "status": true,
        "height": 167.0,
        "volume_parameters": {
            "hips": 86.9378,
            "chest": 82.8079,
            "waist": 72.8193,
            "neck": "36.6641",
            "test_data": {
                "hips": "86.9378",
                "waist": "72.8193",
                "WAIST_M2_MOD": "0",
                "NECK_CH_MOD": "48.7102",
                "NECK_M1_MOD": "36.6641",
                "CHEST_TOP_CON_MOD": "69.0166",
                "model_url": "/wp-content/themes/startit/assets/assets/model.obj",
                "WAIST_M1_MOD": "77.7432",
                "chest": "82.8079",
                "CHEST_START_CON_MOD": "87.9612",
                "COMLEXITY_COEFF": "19.2566",
                "THORAX_M0_MOD": "83.5005",
                "COMPLEXITY": "-1",
                "neck": "36.6641",
                "getFrontSideSum": "153.315",
                "CROCH_M1_MOD": "0",
                "THORAX_M1_MOD": "0",
                "CROCH_M0_MOD": "0",
                "getTypeOfBody": "0"
            }
        },
        "body_height": 51.25758315740174,
        "parameters": {
            "front": {
                "hips": 32.718936565174396,
                "waist": 25.356513319690976,
                "body_height": 51.25758315740174,
                "chest": 27.677575025903817,
                "legs_height": 91.29544845728232,
                "shoulders": 38.98521486647622
            },
            "volume": {
                "hips": 86.9378,
                "chest": 82.8079,
                "waist": 72.8193
            },
            "side": {
                "hips": 22.62839481123336,
                "chest": 24.7199034781482,
                "waist": 20.213690483063832
            }
        }
    };

    var API_KEY = '<Config:apiKey>';
    var API_ENDPOINT = '<Config:apiEndpoint>';
    var FIELDS = {
        'volume_params.chest': 'Chest',
        'volume_params.waist': 'Waist',
        'volume_params.high_hips': 'Hips',
        'volume_params.neck': 'Neck',
        'front_params.shoulders': 'Shoulders width',
        'front_params.chest': 'Front chest width',
        'front_params.waist': 'Front waist width',
        'front_params.high_hips': 'Front hips width',
        'front_params.shoulder_to_waist': 'Shoulders to waist length',
        'front_params.sleeve_length': 'Sleeve length',
        'front_params.body_height': 'Body length',
        'front_params.legs_height': 'Legs length',
        'front_params.hip_height': 'Waist-to-hip length',
        'front_params.crotch_length': 'Urise (Crotch) length',
        'front_params.hem': 'Hem width',
        'front_params.forearm': 'Forearm',
        'front_params.jacket_length': 'Jacket Lenght',
        'front_params.nape_to_waist_centre_back': 'Nape to Waist Centre Back',
        'front_params.scye_depth': 'Scye Depth',
        'front_params.shoulder_length': 'Shoulder Length',
        'side_params.chest': 'Side chest width',
        'side_params.waist': 'Side waist width',
        'side_params.high_hips': 'Side hips width',
        'front_params.inseam': 'Inseam',
        'front_params.outseam': 'Outseam',
        'side_params.nape_to_bust': 'Nape to Bust',
        'side_params.nape_to_waist_over_bust': 'Nape to Waist over Bust'
    };

    var common = saia.common;
    var Model = saia.core.Model;
    var HeightInput = saia.core.HeightInput;
    var GenderSwitcher = saia.core.GenderSwitcher;
    var Photo = saia.core.Photo;
    var PhotoGroup = saia.core.PhotoGroup;
    var Uploader = saia.core.Uploader2;

    var isInfoSubmitted = false;
    var skipHeightInputFocus = false;
    var model = getModel();
    var uploader = getUploader(model);

    var heightInput = new HeightInput(
        $('.model-creator .height'),
        $('.model-creator .height-input-cm'),
        $('.model-creator .height-input-ft'),
        $('.model-creator .height-input-in'),
        $('.model-creator .cm'),
        $('.model-creator .in'),
        'in'
    );

    var genderSwitcher = new GenderSwitcher(
        $('.model-creator .gender-female'),
        $('.model-creator .gender-male')
    );

    var frontPhoto = new Photo(
        $('.model-creator .front-frame'),
        $('.model-creator .front-frame .photo-wrapper'),
        $('.model-creator .front-frame .photo-wrapper .drag-overlay'),
        $('.model-creator .front-frame .photo-wrapper .invalid-photo-overlay'),
        $('.model-creator .front-frame .photo-wrapper .photo')
    );

    var sidePhoto = new Photo(
        $('.model-creator .side-frame'),
        $('.model-creator .side-frame .photo-wrapper'),
        $('.model-creator .side-frame .photo-wrapper .drag-overlay'),
        $('.model-creator .side-frame .photo-wrapper .invalid-photo-overlay'),
        $('.model-creator .side-frame .photo-wrapper .photo')
    );

    var photoGroup = new PhotoGroup(
        $('.model-creator .photos-wrapper'), frontPhoto, sidePhoto
    );

    var innerWrapperElement = $('.model-creator .inner-wrapper');
    var modelViewerElement = $('.model-creator .model-viewer');
    var modelViewerContainerElement = $('.model-creator .model-viewer .container');
    var parametersElement = $('.model-creator .model-viewer .parameters-wrapper .parameters');
    var unitCm = $('.model-creator .model-viewer .parameters-wrapper .unit-switcher .cm');
    var unitInch = $('.model-creator .model-viewer .parameters-wrapper .unit-switcher .inch');

    var modelViewer = null;
    var receivedResponse = null;
    var currentUnit = null;

    function getModelURL(response) {
        return _.get(response, 'volume_params.body_model');
    }

    function getModel() {

        return new Model({
            height: 0,
            gender: 'female',
            frontPhoto: null,
            sidePhoto: null,
            name: '',
            company: '',
            email: ''
        });
    }

    function getUploader(model) {

        return new Uploader(API_KEY, API_ENDPOINT, model);
    }

    function showLoader(label) {

        $('.model-creator .loader-label').text(label || '');
        $('.model-creator .loader').removeClass('hidden');
    }

    function hideLoader() {

        $('.model-creator .loader').addClass('hidden');
    }

    function submitInfo() {

        if (DEVELOPMENT) {

            return $.when();
        }

        return $.Deferred(function (deferred) {

            isInfoSubmitted = true;

            var xhr = new XMLHttpRequest();

            xhr.open('POST', 'https://saia.3dlook.me/companies/?type=3dmodel', true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.addEventListener('load', function () {

                if (xhr.status < 200 || xhr.status >= 300) {

                    deferred.reject('Unable to send info');
                    return;
                }

                deferred.resolve();
            });
            xhr.send(JSON.stringify({
                name: model.get('company') || '<NO_NAME>',
                contact_person: model.get('name'),
                email: model.get('email')
            }));
        }).promise();
    }

    function create3DModel() {
        if (DEVELOPMENT) {

            _.set(DEV_RESPONSE, 'volume_parameters.test_data.model_url', '/wp-content/themes/startit/assets/assets/' + model.get('gender') + '.obj');

            return $.when(DEV_RESPONSE);
        }

        return uploader.upload();
    }

    function sendFile(email, fileURL) {

        return $.Deferred(function (deferred) {

            var xhr = new XMLHttpRequest();

            xhr.open('GET', fileURL, true);
            xhr.responseType = 'blob';
            xhr.addEventListener('load', function () {

                if (xhr.status < 200 || xhr.status >= 300) {

                    deferred.reject('Unable to receive file');
                    return;
                }

                var formData = new FormData();
                formData.append('email', email);
                formData.append('file', xhr.response);

                (function () {

                    var xhr = new XMLHttpRequest();

                    xhr.open('POST', 'https://saia.3dlook.me/companies/send-model-file/', true);
                    xhr.addEventListener('load', function () {

                        if (xhr.status < 200 || xhr.status >= 300) {

                            deferred.reject('Unable to send file');
                            return;
                        }

                        deferred.resolve();
                    });
                    xhr.send(formData);
                })();
            });
            xhr.send();
        }).promise();
    }

    function validateParams() {

        if (DEVELOPMENT) {

            return $.when();
        }

        return $.Deferred(function (deferred) {

            if (!heightInput.isValid()) {

                deferred.reject('Please provide correct height');
                return;
            }

            if (!frontPhoto.isValid()) {

                deferred.reject('Please provide valid front photo');
                return;
            }

            if (!sidePhoto.isValid()) {

                deferred.reject('Please provide valid side photo');
                return;
            }

            deferred.resolve();
        }).promise();
    }

    function isInfoFormInvalid() {

        if (DEVELOPMENT) {

            return false;
        }

        return $('.model-creator form.model-creator-form').is(':invalid');
    }

    function loadModelViewer() {

        return $.Deferred(function (deferred) {

            if (saia.ModelViewer) {

                deferred.resolve();
            } else {

                var loadScript = function (src) {

                    return $.Deferred(function (deferred) {

                        var script = document.createElement('script');
                        script.setAttribute('type', 'application/javascript');
                        script.setAttribute('src', src);
                        script.addEventListener('load', function () {

                            deferred.resolve();
                        });
                        script.addEventListener('error', function (e) {

                            deferred.reject(e);
                        });
                        document.getElementsByTagName('head')[0]
                            .appendChild(script);
                    }).promise();
                };

                $.when(
                    loadScript('/wp-content/themes/startit/assets/js/model-viewer.js')
                ).then(function () {

                    deferred.resolve();
                }, function (e) {

                    deferred.reject(e);
                });
            }
        });
    }

    function formatParameter(value, unit) {

        switch (unit) {
            case 'cm':
                return Math.round(+value) + ' cm';
            case 'inch':
                return +value ? +(+value / 2.54).toFixed(2) + '"' : '0.00\'';
        }
    }

    function updateParameters() {

        if (receivedResponse) {

            parametersElement.empty();

            _.each(FIELDS, function (name, path) {

                var value = _.get(receivedResponse, path);
                if (_.isUndefined(value)) {

                    return;
                }

                $('' +
                    '<div class="parameter">' +
                    '   <div class="parameter-name">' + name + '</div>' +
                    '   <div class="parameter-value">' + formatParameter(value, currentUnit) + '</div>' +
                    '</div>' +
                    ''
                ).appendTo(parametersElement);
            });
        }
    }

    function switchUnit(unit) {

        if (currentUnit !== unit) {

            switch (unit) {
                case 'cm': {
                    unitCm.addClass('active');
                    unitInch.removeClass('active');
                    break;
                }
                case 'inch': {
                    unitInch.addClass('active');
                    unitCm.removeClass('active');
                    break;
                }
            }

            currentUnit = unit;
            updateParameters();
        }
    }

    unitCm.click(function () {

        switchUnit('cm');
    });

    unitInch.click(function () {

        switchUnit('inch');
    });

    heightInput.on('change', function (height) {

        model.set('height', height);
    });

    heightInput.on('unit:change', function () {

        if (skipHeightInputFocus) {

            skipHeightInputFocus = false;
            return;
        }

        heightInput.focus();
    });

    genderSwitcher.on('change', function (gender) {

        model.set('gender', gender);
    });

    $('.model-creator .height-input')
        .focus(function () {

            $('.model-creator .height-input-wrapper').addClass('focus');
        })
        .blur(function () {

            $('.model-creator .height-input-wrapper').removeClass('focus');
        });

    $('.model-creator #min-photo-size').text(
        common.formatFileSize(Photo.MIN_PHOTO_SIZE)
    );

    $('.model-creator #max-photo-size').text(
        common.formatFileSize(Photo.MAX_PHOTO_SIZE)
    );

    [frontPhoto, sidePhoto].forEach(function (photo) {

        photo.on('change', function (loadTask) {

            loadTask.fail(function (e) {

                alert(e);
            });

            common.deferredTask(loadTask, function () {

                showLoader('Loading photo');
            }, 250).then(function () {

                hideLoader();
            });
        });
    });

    frontPhoto.on('load', function () {

        model.set('frontPhoto', frontPhoto.binaryData);
    });

    sidePhoto.on('load', function () {

        model.set('sidePhoto', sidePhoto.binaryData);
    });

    if (template) {

        $('.model-creator .template')
            .removeClass('hidden')
            .click(function () {

                var genderTemplate = template[model.get('gender')];

                skipHeightInputFocus = true;
                model.set('height', genderTemplate.height);
                heightInput.setUnit('cm', genderTemplate.height);

                common.deferredTask(
                    $.when(
                        frontPhoto.fromURL(genderTemplate.front, 'front'),
                        sidePhoto.fromURL(genderTemplate.side, 'side')
                    ), function () {

                        showLoader('Loading template');
                    }, 250
                ).then(function () {

                    hideLoader();
                }, function () {

                    hideLoader();
                    setTimeout(function () {

                        alert('Unable to load template');
                    }, 0);
                });
            });
    }

    $('.model-creator .inputs .input').on('input', function () {

        var input = $(this);

        if (input.is(':invalid') && (input.val() || '').length !== 0) {

            input.addClass('invalid');
        } else {

            input.removeClass('invalid');
        }

        model.set(input.attr('name'), input.val());
    });

    $('.model-creator .continue-button').click(function () {

        if (USAGE_LIMIT > 0) {

            var requestsCount = +localStorage.getItem('requestsCount') || 0;
            if (requestsCount >= USAGE_LIMIT) {

                var lastRequestTimestamp = +localStorage.getItem('lastRequestTimestamp') || 0;

                if ((_.now() - lastRequestTimestamp) >= USAGE_LIMIT_DELAY) {

                    localStorage.setItem('requestsCount', 0);
                } else {

                    alert('Usage limit exceeded. Try again tomorrow.');
                    return;
                }
            }
        }

        if (isInfoFormInvalid()) {

            return;
        }

        validateParams().then(function () {

            showLoader('Doing magic. It takes up to 30 seconds');
            return submitInfo().then(function () {

                return create3DModel();
            }).then(function (response) {

                if (USAGE_LIMIT > 0) {

                    var requestsCount = +localStorage.getItem('requestsCount') || 0;

                    localStorage.setItem('requestsCount', requestsCount + 1);
                    localStorage.setItem('lastRequestTimestamp', _.now());
                }

                if (!saia.ModelViewer) {

                    showLoader('Model viewer is being loaded');
                }

                loadModelViewer().then(function () {

                    receivedResponse = response;
                    modelViewer = new saia.ModelViewer();

                    innerWrapperElement.addClass('hidden');
                    modelViewerElement.removeClass('hidden');

                    if (common.isMobileDevice()) {

                        modelViewerContainerElement.css({
                            height: ($(window).height() / 2) + 'px'
                        });
                    }

                    updateParameters();

                    var modelCreatorElement = $('.model-creator');
                    if (modelCreatorElement.hasClass('full-screen')) {

                        $(window).scrollTop(0);
                    } else {

                        if (window.matchMedia('(max-width: 439px)').matches) {

                            modelCreatorElement[0].scrollIntoView(true);
                        }
                    }

                    parametersElement.scrollTop(0);

                    showLoader('Model is being loaded');
                    modelViewer.init(modelViewerContainerElement);

                    modelViewer.load(
                        _.get(response, 'volume_params.body_model')
                    ).then(function () {

                        hideLoader();
                    }, function () {

                        hideLoader();
                        setTimeout(function () {

                            alert('Unable to load model');
                        });
                    });
                }, function () {

                    hideLoader();
                    setTimeout(function () {

                        alert('Unable to load model viewer');
                    });
                });
            }, function (e) {

                hideLoader();
                setTimeout(function () {

                    handleUploadError(e);
                });
            });
        }, function (e) {

            alert(e);
        });
    });

    function handleUploadError(e) {
        if (e && e.body && e.body.sub_tasks) {
            var subTasks = e.body.sub_tasks;
            var front = subTasks.filter(function (s) { return s.name.indexOf('front_') !== -1 })[0];
            var side = subTasks.filter(function (s) { return s.name.indexOf('side_') !== -1 })[0];

            var errorMessage = '';

            if (front.status === 'FAILURE') {
                errorMessage += 'Front photo: ' + front.message + '\n'
            }

            if (side.status === 'FAILURE') {
                errorMessage += 'Side photo: ' + side.message + '\n'
            }

            if (errorMessage) {
                alert(errorMessage);
            }
        } else {
            alert('Error');
        }
    }

    $('.model-creator .model-viewer .download-button').click(function () {

        if (!modelViewer || !receivedResponse) {

            return;
        }

        window.open(getModelURL(receivedResponse), '_self');
    });

    if (sendViaEmailDisabled) {

        $('.model-creator .model-viewer .email-button').hide();
    } else {

        $('.model-creator .model-viewer .email-button').click(function () {

            if (!modelViewer || !receivedResponse) {

                return;
            }

            showLoader('Sending file...');

            sendFile(
                model.get('email'),
                getModelURL(receivedResponse)
            ).then(function () {

                hideLoader();
                setTimeout(function () {

                    alert('File successfully has been sent');
                }, 0);
            }, function (e) {

                hideLoader();
                setTimeout(function () {

                    alert(e);
                }, 0);
            });
        });
    }

    $('.model-creator .model-viewer .reload-button').click(function () {

        if (modelViewer) {

            modelViewer.destroy();
            modelViewer = null;
            receivedResponse = null;

            model = getModel();
            uploader = getUploader(model);

            genderSwitcher.clear();
            heightInput.clear();
            photoGroup.clear(true);

            $('.inputs .input').each(function () {

                $(this).val('');
                $(this).removeClass('invalid');
            });

            innerWrapperElement.removeClass('hidden');
            modelViewerElement.addClass('hidden');
        }
    });

    if (common.isMobileDevice()) {

        $('.model-creator').addClass('mobile');

        heightInput.focus();
    }

    $(window).on('load', function () {

        $('.model-creator').addClass('loaded');
    });

    $(window).on('resize', function () {

        if (common.isMobileDevice()) {

            modelViewerContainerElement.css({
                height: ($(window).height() / 2) + 'px'
            });
        }
    });

    if (env === 'dev') {

        $('.model-creator .header').click(function () {
            location.reload(true);
        });
    }

    switchUnit('cm');

})('<Config:env>', window.saia);
